package bean;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConn {
	// 오라클 데이터 베이스 연동 과정
	private String driver = "oracle.jdbc.driver.OracleDriver"; // 고정
	private String path = "jdbc:oracle:thin:@192.168.0.16:1521:ORCL"; // 고정(서버설정
																	// 정보에따라 상이)
	private String user = "JHTA1604_JO2";
	private String pwd = "1111"; // 오라클 사용할때 사용하는 ID/비번

	private Connection conn; // DB 연결정보 를 기억

	public DBConn() {
		try {
			// 1번째 작업:드라이버 로딩
			Class.forName(driver).newInstance();
			// DB 연결
			conn = DriverManager.getConnection(path, user, pwd);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Connection getConn() {
		return conn;
	}

	public static void main(String[] args) {
		new DBConn();
	}
}
