package bean;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

public class AcademyDao {
	
	private String lk_upPath = "C:/workspecs/final/WebContent/academy/images";
	private int size = 10*1024*1024;
	private String lk_oldFile="";
	int category_val;
	int tsearch ;
	
	int listSize = 3; 
	int blockSize = 4;
	
	int totSize = 0; 
	int totPage = 0;
	
	int totBlock = 0;  
	int nowBlock = 0;  
	
	int nowPage = 1;   
	int startNo = 0;  // endNo - listSize + 1 startNo = endNo - listSize + 1
	int endNo = 0;  // nowPage * listSize   endNo = nowBlock * blockSize
	
	int endPage = 0;  // nowBlock * blockSize   endPage = nowBlock * blockSize
	int startPage = 0;  // endPage - blockSize + 1   startPage = endPage - blockSize + 1
	
	public AcademyDao() {
		// TODO Auto-generated constructor stub
	}
	
	public List<AcademyVo> list() {
		List<AcademyVo> list = new ArrayList<AcademyVo>();
		
		String sql = "";
		PreparedStatement ps = null; 
		ResultSet rs = null;
		
		try{
			Connection conn = new DBConn().getConn();
			
			sql = "select * from lk_academy"
					
					+ " order by lk_date";
			
			ps = conn.prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				AcademyVo v = new AcademyVo();
				v.setLk_code(rs.getInt("lk_code"));
				v.setLk_bdoid(rs.getInt("lk_bdoid"));
				v.setLk_scityid(rs.getInt("lk_scityid"));
				v.setLk_name(rs.getString("lk_name"));
				v.setLk_txt(rs.getString("lk_txt"));
				v.setLk_cul(rs.getString("lk_cul"));
				v.setLk_logo(rs.getString("lk_logo"));
				v.setLk_map(rs.getString("lk_map"));
				v.setLk_address(rs.getString("lk_address"));
				v.setLk_manage(rs.getString("lk_manager"));
				v.setLk_date(rs.getDate("lk_date"));
				v.setLk_email(rs.getString("lk_email"));
				v.setLk_phone(rs.getString("lk_phone"));
				//v.setLk_bdoname(rs.getString("doname"));
				//v.setLk_scityname(rs.getString("siname"));
				
				list.add(v);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		} finally{
			return list;
		}
	}
	
	public List<AcademyVo> hak_list() {
    	List<AcademyVo> hak_list = new ArrayList<AcademyVo>();
		
    	PreparedStatement ps = null; 
		ResultSet rs = null;
		String sql = "";
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
				   
				   
				
				  sql = "select * from lk_learning " 
				        + "where s_code is null "				        
				        + "order by l_code asc";		        
				  ps = conn.prepareStatement(sql);			   	
				  
				  rs = ps.executeQuery();
				  
				  while(rs.next()){
					  AcademyVo v = new AcademyVo();
					  
					  v.setL_code(rs.getInt("l_code"));
					  v.setS_code(rs.getInt("s_code"));
					  v.setLearning_name(rs.getString("learning_name"));
					  
					  hak_list.add(v);

					  
					  
				  }
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			
		}
		return hak_list;
	}
	
	public List<AcademyVo> local_list() {
    	List<AcademyVo> local_list = new ArrayList<AcademyVo>();
		
    	PreparedStatement ps = null; 
		ResultSet rs = null;
		String sql = "";
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
				   
				   
				  
				  sql = "select * from lk_local " 
				        + "where s_cityid is null "				        
				        + "order by l_doid asc";		        
				  ps = conn.prepareStatement(sql);			   	
				  
				  rs = ps.executeQuery();
				  
				  while(rs.next()){
					  AcademyVo v = new AcademyVo();
					  
					  v.setL_doid(rs.getInt("l_doid"));
					  v.setS_cityid(rs.getInt("s_cityid"));
					  v.setLocal_name(rs.getString("local_name"));
					  
					  local_list.add(v);

					  
					  
				  }
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			
		}
		return local_list;
	}	

	public List<AcademyVo> license_list() {
    	List<AcademyVo> license_list = new ArrayList<AcademyVo>();
		
    	PreparedStatement ps = null; 
		ResultSet rs = null;
		String sql = "";
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
				   
				   
				  
				  sql = "select * from lk_license " 
				        + "where s_license is null "				        
				        + "order by l_license asc";		        
				  ps = conn.prepareStatement(sql);			   	
				  
				  rs = ps.executeQuery();
				  
				  while(rs.next()){
					  AcademyVo v = new AcademyVo();
					  
					  v.setL_license(rs.getInt("l_license"));
					  v.setS_license(rs.getInt("s_license"));
					  v.setLicense_name(rs.getString("license_name"));
					  
					  license_list.add(v);

					  
					  
				  }
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			
		}
		return license_list;
	}		
	
	public List<AcademyVo> category() {
    	List<AcademyVo> ct_list = new ArrayList<AcademyVo>();
		
    	PreparedStatement ps = null; 
		ResultSet rs = null;
		String sql = "";
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
				   
				   
				  
				  sql = "select * from lk_study "
				  		+ "where l_hak=? and s_hak is not null";		        
				  ps = conn.prepareStatement(sql);
				  ps.setInt(1, category_val);
				  
				  rs = ps.executeQuery();
				  
				  while(rs.next()){
					  AcademyVo v = new AcademyVo();
					  
					  v.setL_hak(rs.getInt("l_hak"));
					  v.setS_hak(rs.getInt("s_hak"));
					  v.setHak_name(rs.getString("hak_name"));
					  
					  ct_list.add(v);

					  
					  
				  }
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			
		}
		return ct_list;
	}

	public List<AcademyVo> tk_list() {
		List<AcademyVo> tk_list = new ArrayList<AcademyVo>();
		
		PreparedStatement ps = null; 
		ResultSet rs = null;
		String sql = "";
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
			
			sql = "select count(*) totSize from lk_academy " 
				+ "where lk_scityid=? or ";
		} catch(Exception ex){
			ex.printStackTrace();
		} finally{
		}
		return tk_list;
	}
	
	public boolean isNum(String str1, String str2){
		boolean b=true;
		try{
			double d = Double.parseDouble(str1);
			double e = Double.parseDouble(str2);
		}catch(Exception ex){
			b=false;
		}finally{
			return b;
		}
	}
	
	public AcademyVo lk_view(int lk_code){
		AcademyVo vo = new AcademyVo();
		String sql ="";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
			
			sql = "select *from lk_academy where lk_code=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, lk_code);
			rs = ps.executeQuery();	
			
			while(rs.next()){
			
			vo.setLk_code(rs.getInt("lk_code"));
			vo.setLk_bdoid(rs.getInt("lk_bdoid"));
			vo.setLk_scityid(rs.getInt("lk_scityid"));
			vo.setLk_name(rs.getString("lk_name"));
			vo.setLk_txt(rs.getString("lk_txt"));
			vo.setLk_cul(rs.getString("lk_cul"));			
			vo.setLk_logo(rs.getString("lk_logo"));			
			vo.setLk_map(rs.getString("lk_map"));	
			vo.setLk_address(rs.getString("lk_address"));
			vo.setLk_manage(rs.getString("lk_manager"));
			vo.setLk_date(rs.getDate("lk_date"));
			vo.setLk_email(rs.getString("lk_email"));
			vo.setLk_phone(rs.getString("lk_phone"));
			vo.setLk_home(rs.getString("lk_home"));
			
			
			}
		}catch(Exception ex){
			ex.printStackTrace();
			rs.close();
		}finally{			
			return vo;
		}
	}
	

	public List<AcademyVo> t_list(int lk_code){
		List<AcademyVo> list = new ArrayList<AcademyVo>();
		   String sql = "";
		   PreparedStatement ps= null;
		   ResultSet rs = null;
		try{
			DBConn db = new DBConn();
			Connection conn = db.getConn();
			
			  // totSize를 위한 쿼리
			   sql = "select count(*) totSize from lk_teacher "
				       	+ " where lk_tserial  like ? "				   	  	
				        + " or lk_tcontent like ? ";
			       
			      ps = conn.prepareStatement(sql);
			      ps.setString(1,"%" + tsearch + "%");
			      ps.setString(2,"%" + tsearch + "%");
			      
			      		       
			      rs = ps.executeQuery();
			      rs.next();
			      totSize = rs.getInt("totSize");
			       
			   // 페이지 분리를 위한 변수 계산
			   			      
			      totPage = (int)Math.ceil(totSize/(float)listSize); 
			      totBlock = (int)Math.ceil(totPage/(float)blockSize); 
			      nowBlock = (int)Math.ceil(nowPage/(float)blockSize); 
			          
			      endNo = nowPage * listSize; 
			      startNo = endNo - listSize + 1; 
			          
			      endPage = nowBlock * blockSize; 
			      startPage = endPage - blockSize +1; 
			       
			    //전체수보다 계산된 수가 더 클경우 보정 
			     if(endPage > totPage) endPage = totPage;
			
			sql = "select * from( select rownum no, s. * from(select *from lk_teacher"
					+ " where lk_code = ? order by lk_tserial desc )s )where no between ? and ?";
			
			ps =conn.prepareStatement(sql);
			ps.setInt(1, lk_code );
			ps.setInt(2, startNo);
		    ps.setInt(3, endNo);
		    
		    rs = ps.executeQuery();
		    
		    while(rs.next()){
		    	AcademyVo v = new AcademyVo();
		    	
		    	v.setLk_tserial(rs.getInt("lk_tserial"));
		    	v.setLk_code(rs.getInt("lk_code"));
		    	v.setLk_timage(rs.getString("lk_timage"));
		    	v.setLk_tcontent(rs.getString("lk_tcontent"));
		    	list.add(v);
		    }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			return list;
		}
	}
		
	
		
	// delete 
		public int delete(int lk_code){
			int r =0;
			String sql = "";
			PreparedStatement ps= null;
			File file = null;
			
			try{
				DBConn db = new DBConn();
				Connection conn = db.getConn();
				
				// <�뜝�럡�뀭�뜝�럩�젷�뜝�럥留� �뜝�럥�냱�뜝�럩逾х춯瑜낆삕 占쎈퓛占쎈�獄�琉꾩삕�뇡占썲뜝�럥裕� 占쎄턀�겫�뼔援�>
				//�뜝�럡�뀭�뜝�럩�젷�뜝�럥留� �뜝�럥�냱�뜝�럩逾х춯琉욧턂占쎈굵 �뜝�럩�쓧�뜝�럥堉� �뛾�룇猷꾤뵳占� �뜝�럩留꾢뜝�럥�돵�뜝�럡�맋 �뜝�럥�냱�뜝�럩逾ф쾬�꼶梨멱땻節뉖ご�뜝占� �뛾�룇猷뉔뇡�꼻�삕�땻占� �뜝�럩�굚�뜝�럥�뵜�뜝�럥�돵�뜝�럥利� �뜝�럥彛녶뜝�럥堉�.
				// n�뤆�룇裕뉛옙踰� �뜝�럥�냱�뜝�럩逾э옙紐닷뜝占� 嶺뚳퐘維��뜝�룞�삕�뇡占� 占쎄턀�겫�뼔援▼뜝�럩逾� �뇦猿뗫윪占쎈뮡�뜝�럥�뱺�뜝�럥裕� �뛾�룆理뚳옙沅� �뤆�룇�듌占쎈턄 �뜝�럡�뀭�뜝�럩�젷�뜝�럥六삣뜝�럡�뀞�뜝�럥裕� �뛾�렮維뽬떋�슱�삕占쎈굵 �뜝�럡�뀬�뜝�럩�뮔�뜝�럥由��뜝�럥裕� �뇦猿됲뜑占쎈턄 �뜝�럩占쏙옙逾놂옙�뿫由��뜝�럥堉�.
				/*sql = " select * from lk_teacher where lk_code= ? ";
				        
				ps = conn.prepareStatement(sql);
				ps.setInt(1, lk_code);	
				
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
		    	   
				 if(rs.getString("file1") != null){
		            file = new File(lk_upPath+ "/" +rs.getString("file1"));
		            if(file.exists()) file.delete();
		         }
		         if(rs.getString("file2") != null){
		            file = new File(lk_upPath+ "/" +rs.getString("file2"));
		            if(file.exists()) file.delete();
		         }
		         if(rs.getString("file3") != null){
		            file = new File(lk_upPath+ "/" +rs.getString("file3"));
		            if(file.exists()) file.delete();
		         }
			 }*/
				
				// �뜝�럥由겼뜝�럩�쐸占쎄턀�겫�뼔援∽옙紐닷뜝占� �뛾�룇猷뉔뇡�꼻�삕�땻占� DB(lk_academy) �뜝�럡�뀭�뜝�럩�젷�뜝�럥由썲뜝�럥堉�.(�뜝�럥�돵�뜝�럥堉� row占쎈ご�뜝占� �뜝�럡�뀭�뜝�럩�젷�뜝�럥由��뜝�럥裕� �뜝�럩�읉嶺뚢댙�삕)
				sql = " delete from lk_academy where lk_code = ? "; 
				ps = conn.prepareStatement(sql);
				ps.setInt(1, lk_code); 				
				
				r = ps.executeUpdate();
				
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				return r;
			}
			
		} 		
	
		

		/*1604 이현재 로직 입니다************************************************************************************/
		//수도권 대분류********************************************************************
		public List<AcademyVo> Local(){
			List<AcademyVo> list = new ArrayList<AcademyVo>();
			
			String sql = "";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try{ 
				Connection conn = new DBConn().getConn();

				//테이블 정보 출력
				sql="select * from lk_Local "
				  + "where s_cityid is null "
				  + "order by Local_name asc";
				
				ps = conn.prepareStatement(sql);			   	
				  
				  rs = ps.executeQuery();
				
				while(rs.next()){
					AcademyVo v = new AcademyVo();
					 v.setL_doid(rs.getInt("l_doid"));
					  v.setLocal_name(rs.getString("Local_name"));
					  list.add(v);
				}
			}catch (Exception ex){
				ex.printStackTrace();
			}finally{
				return list;
			}
		}
		
		//지역 modify에서 소분류 뿌려주기****************************************
			public List<AcachkiVo1> Locals(int lcode){
				List<AcachkiVo1> los = new ArrayList<AcachkiVo1>();
				String sql="";
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					
					Connection conn = new DBConn().getConn();
					sql="select * from lk_Local where l_doid=? and s_cityid is not null"; // 지역
					ps = conn.prepareStatement(sql);
					ps.setInt(1,  lcode);
					rs = ps.executeQuery();
					
					while(rs.next()){
						AcachkiVo1 v = new AcachkiVo1();
						v.setS_cityid(rs.getInt("s_cityid"));
						v.setLocal_name(rs.getString(("Local_name")));
						los.add(v);
					}
			}catch (Exception ex){
				ex.printStackTrace();
			}finally{
				return los;
			}
			}
		
		//학문 대분류********************************************************************
		public List<AcademyVo> study(){
			List<AcademyVo> list = new ArrayList<AcademyVo>();
			
			String sql = "";
			PreparedStatement ps = null;
			ResultSet rs = null;

			try{ 
				Connection conn = new DBConn().getConn();

				//테이블 정보 출력
				sql="select * from lk_learning "
				  + "where s_code is null "
				  + "order by learning_name asc";
				
				ps = conn.prepareStatement(sql);			   	
				  
				  rs = ps.executeQuery();
				
				while(rs.next()){
					AcademyVo v = new AcademyVo();
					  v.setL_code(rs.getInt("l_code"));
					  v.setLearning_name(rs.getString("learning_name"));
					  list.add(v);
				}
			}catch (Exception ex){
				ex.printStackTrace();
			}finally{
			}
			return list;
		}
		//학문 modify에서 소분류 뿌려주기****************************************
		public List<AcachkiVo1> mody1(int code){
			List<AcachkiVo1> rvo1 = new ArrayList<AcachkiVo1>();
			String sql="";
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				Connection conn = new DBConn().getConn();
				sql="select * from lk_LEARNING where l_code=? and s_code is not null order by learning_name asc"; // 학문
				ps = conn.prepareStatement(sql);
				ps.setInt(1,  code);
				rs = ps.executeQuery();
				
				while(rs.next()){
					AcachkiVo1 v02 = new AcachkiVo1();
					v02.setS_code(rs.getInt("s_code"));
					v02.setLearning_name(rs.getString(("learning_name")));
					rvo1.add(v02);
				}
		}catch (Exception ex){
			ex.printStackTrace();
		}finally{
			return rvo1;
		}
		}
		
		//자격증 대분류********************************************************************
			public List<AcademyVo> license(){
				List<AcademyVo> list = new ArrayList<AcademyVo>();
				
				String sql = "";
				PreparedStatement ps = null;
				ResultSet rs = null;

				try{ 
					Connection conn = new DBConn().getConn();

					//테이블 정보 출력
					sql="select * from lk_license "
					  + "where s_license is null "
					  + "order by license_name asc";
					
					ps = conn.prepareStatement(sql);			   	
					  
					  rs = ps.executeQuery();
					
					while(rs.next()){
						AcademyVo v = new AcademyVo();
						
						 v.setL_license(rs.getInt("l_license"));
						  v.setS_license(rs.getInt("s_license"));
						  v.setLicense_name(rs.getString("license_name"));
						  
						  list.add(v);
					}

				}catch (Exception ex){
					ex.printStackTrace();
				}finally{
				}
				return list;
			}
			
			//자격증 modify에서 소분류 뿌려주기****************************************
			public List<AcachkiVo1> mody2(int code){
				List<AcachkiVo1> rvo1 = new ArrayList<AcachkiVo1>();
				String sql="";
				PreparedStatement ps = null;
				ResultSet rs = null;
				
				try {
					Connection conn = new DBConn().getConn();
					sql="select * from lk_license where l_license=? and s_license is not null order by license_name asc"; // 학문
					ps = conn.prepareStatement(sql);
					ps.setInt(1,  code);
					rs = ps.executeQuery();
					
					while(rs.next()){
						AcachkiVo1 v02 = new AcachkiVo1();
						v02.setS_license(rs.getInt("s_license"));
						v02.setLicense_name(rs.getString("license_name"));
						rvo1.add(v02);
						
					}

			}catch (Exception ex){
				ex.printStackTrace();
			}finally{
				return rvo1;
			}
			}
			// input 데이터 값 받고 result에 결과 보여주기전(insert)************************
			public int insert(HttpServletRequest request){
				int r=0;
				
				
				String sql = null;
				PreparedStatement ps = null;
				Connection conn;
				ResultSet rs = null;
				String file1="";
				String delFile="";//삭제될파일
				 String dftFilePath = request.getServletContext().getRealPath("/");
				 lk_upPath = dftFilePath+"academy/images";
				try{
					MultipartRequest multi=new MultipartRequest(
							request,
							lk_upPath,
							size,
							"utf-8",
							new DefaultFileRenamePolicy());
					
					int lee_l_lo = Integer.parseInt(multi.getParameter("lee_l_lo")); //지역(대)
					int lee_s_lo =  Integer.parseInt(multi.getParameter("lee_s_lo")); //지역(소)
					String lee_sName = multi.getParameter("lee_sName"); //학원이름
					String lee_hakreview = multi.getParameter("lee_hakreview"); //학원소개
					String lee_sCurriculum = multi.getParameter("lee_sCurriculum"); //커리큘럼
					String lee_sFile = multi.getFilesystemName("lee_sFile"); //학원사진
					//위치주소?
					String lee_sAddress = multi.getParameter("lee_sAddress"); //학원 주소
					String mid = multi.getParameter("im_id"); // 관리자ID
					//등록일
					String lee_sEmail = multi.getParameter("lee_sEmail"); //학원이메일
					String lee_sPhone = multi.getParameter("lee_sPhone"); //학원 전화번호
					String lee_home = multi.getParameter("lk_home"); // 학원 홈페이지 주소
					
					String i1 = lee_hakreview.replace("\r\n", "<br>");
					String i2 = lee_sCurriculum.replace("\r\n", "<br>");
					
					//첨부파일 저리
					Enumeration e= multi.getFileNames();
					
					
					
					while(e.hasMoreElements()){
						String f = (String)e.nextElement();
						// 사진의 순서를 정할때 만드는 로직 업로드하고 기존파일을 삭제
						if(f.equals("lee_sFile")){
							file1 = multi.getFilesystemName(f);
							delFile = multi.getParameter("oldlee_sFile");
							File file = new File(lk_upPath +"/"+ delFile);
							if(file.exists() && file1 != null){
								file.delete();
							}
						}
					}			
					
					conn = new DBConn().getConn();
					
					
					sql = "insert into lk_academy values(seq_lk_academy.nextval,?,?,?,?,?,?,?,?,?,sysdate,?,?,?)";
					ps = conn.prepareStatement(sql);
//													학원 코드
					ps.setInt(1, lee_l_lo); 		//지역(대)
					ps.setInt(2, lee_s_lo); 		//지역(소)
					ps.setString(3, lee_sName); 	//학원 이름
					ps.setString(4, i1); 			//학원 소개
					ps.setString(5, i2);			//커리큘럼F
					ps.setString(6, lee_sFile); 	//학원사진
					ps.setString(7, lee_sAddress); 	//위치주소?
					ps.setString(8, lee_sAddress);  //학원 주소
					ps.setString(9, mid);		  	//관리자 ID
//												       등록일"
					ps.setString(10, lee_sEmail); 	//학원이메일
					ps.setString(11, lee_sPhone); 	//학원 전화번호
					ps.setString(12, lee_home); 	//학원 홈페이지 주소
					
					
					r = ps.executeUpdate();

					/**************************************************************************/
					for(int i=1; i<8; i++ ){
						String lee_s_li =  multi.getParameter("lee_s_li"+i);//자격증(소)
							
						if(lee_s_li != null){
							int lee_l_li = Integer.parseInt(multi.getParameter("lee_l_li")); //자격증(대)
							int lee_s_lili = Integer.parseInt(lee_s_li);
							
							sql="insert into license values(seq_lk_academy.currval,?,?)";
							ps = conn.prepareStatement(sql);
							ps.setInt(1, lee_l_li);
							ps.setInt(2, lee_s_lili);
							r = ps.executeUpdate();
							
						}
					}
					/**************************************************************************/
					for(int i=1; i<8; i++ ){
						String lee_s_ha = multi.getParameter("lee_s_code"+i); //학문(소)
					
						if(lee_s_ha != null){
						int lee_l_ha = Integer.parseInt(multi.getParameter("lee_l_code")); //학문(대)
						int lee_s_code = Integer.parseInt(lee_s_ha);
						
						sql="insert into learning values(seq_lk_academy.currval,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, lee_l_ha);
						ps.setInt(2, lee_s_code);
						r = ps.executeUpdate();
						}
					}
					/**************************************************************************/
					
					for(int i=0; i<=4; i++ ){
						String lee_tmig = multi.getFilesystemName("lee_tmig"+i); //강사 이미지
						String lee_ttext = multi.getParameter("lee_ttext"+i); //강사 내용
						
						String i3 = lee_ttext.replace("\r\n", "<br>");
						
						if(lee_tmig != null){
							
							//첨부파일 저리
							Enumeration e1= multi.getFileNames();
							while(e1.hasMoreElements()){
								String f = (String)e1.nextElement();
								// 사진의 순서를 정할때 만드는 로직 업로드하고 기존파일을 삭제
								if(f.equals("lee_tmig"+i)){
									file1 = multi.getFilesystemName(f);
									delFile = multi.getParameter("oldlee_tmig"+i);
									File file = new File(lk_upPath +"/"+ delFile);
									if(file.exists() && file1 != null){
										file.delete();
									}
								}
							}
							
							sql="insert into lk_teacher values(seq_lk_teacher.nextval,seq_lk_academy.currval,?,?) ";
							ps = conn.prepareStatement(sql);
							ps.setString(1, lee_tmig);
							ps.setString(2, i3);
							
							r = ps.executeUpdate();
						}
					}
					
				}catch(Exception ex){
					ex.printStackTrace();
				}finally{
					toString();
				}
				return r;
				}
		
			/* 임의의값으로 수정페이지 값 불러오기(메인 테이블)******************************************************** */
			public AcademyVo view(int code, String manager){
				AcademyVo rvo = new AcademyVo();
				
				String sql="";

				try {
					Connection conn = new DBConn().getConn();
					PreparedStatement ps = null;
					ResultSet rs = null;
					
					sql="select * from LK_ACADEMY where lk_code=?";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, code);
					
					rs = ps.executeQuery();
					rs.next();
						rvo.setLk_code(rs.getInt("lk_code"));
						rvo.setLk_bdoid(rs.getInt("lk_bdoid"));
						rvo.setLk_scityid(rs.getInt("lk_scityid"));
						rvo.setLk_name(rs.getString("lk_name"));
						rvo.setLk_logo(rs.getString("lk_logo"));
						rvo.setLk_map(rs.getString("lk_map"));
						rvo.setLk_address(rs.getString("lk_address"));
						rvo.setLk_manage(rs.getString("lk_manager"));
						rvo.setLk_email(rs.getString("lk_email"));
						rvo.setLk_phone(rs.getString("lk_phone"));
						rvo.setLk_home(rs.getString("lk_home"));
						
						String o1 = rs.getString("lk_txt");
						String o2 = rs.getString("lk_cul");
						
						rvo.setLk_txt(o1.replace("<br>","\r\n"));
						rvo.setLk_cul(o2.replace("<br>","\r\n"));
						
						System.out.println("학원소개 ="+rs.getString("lk_txt"));
						System.out.println("커리큘럼 ="+rs.getString("lk_cul"));
					
				}catch (Exception ex){
					ex.printStackTrace();
				}finally{
					return rvo;
				}
			}
			/* 임의의값 불러오기 자격증*******************************************************************************/
	public List<AcachkiVo1> view0(int code){
		List<AcachkiVo1> rvo0 = new ArrayList<AcachkiVo1>();

		String sql="";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection conn = new DBConn().getConn();
		
			sql="select * from LICENSE where lk_code=?"; // 자격증
			ps = conn.prepareStatement(sql);
			ps.setInt(1, code);
			rs = ps.executeQuery();
			
			if(rs.next()){
				AcachkiVo1 v01 = new AcachkiVo1();
				v01.setS_code(rs.getInt("s_code"));
				v01.setL_code(rs.getInt("l_code"));
				rvo0.add(v01);
			}
			
			
		}catch (Exception ex){
			ex.printStackTrace();
		}finally{
			return rvo0;
		}
		
	}
			
			/* 임의의값 불러오기 학문 체크값용*****************************************************************************/
	public List<AcachkiVo1> view1(int code){
		List<AcachkiVo1> rvo1 = new ArrayList<AcachkiVo1>();
		String sql="";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection conn = new DBConn().getConn();
			sql="select * from LEARNING where lk_code=?"; // 학문
			ps = conn.prepareStatement(sql);
			ps.setInt(1, code);
			rs = ps.executeQuery();
			
			if(rs.next()){
				AcachkiVo1 v02 = new AcachkiVo1();
				v02.setL_code(rs.getInt("l_code"));
				v02.setS_code(rs.getInt("s_code"));
				rvo1.add(v02);
				
			}

	}catch (Exception ex){
		ex.printStackTrace();
	}finally{
		return rvo1;
	}
	}

			/* 임의의값 불러오기 강사소개***************************************************************************/
	public List<AcachkiVo1> view2(int code){
		List<AcachkiVo1> rvo2 = new ArrayList<AcachkiVo1>();
		String sql="";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Connection conn = new DBConn().getConn();
			sql="select * from LK_TEACHER where lk_code=?"; //강사소개
			ps = conn.prepareStatement(sql);
			ps.setInt(1, code);
			rs = ps.executeQuery();
			while(rs.next()){
				AcachkiVo1 v03 = new AcachkiVo1();
				v03.setLk_timage(rs.getString("lk_timage"));
				
				String ttext = rs.getString("lk_tcontent");
				ttext.replace("<br>","\r\n");
				
				v03.setLk_tcontent(ttext);
				rvo2.add(v03);
			}
	}catch (Exception ex){
		ex.printStackTrace();
	}finally{
		return rvo2;
	}
	}
			
//			// view 데이터 값 받고 result에 결과 보여주기전(update)************************
			public int update(HttpServletRequest request){
				int r=0;
				
				String sql = null;
				PreparedStatement ps = null;
				Connection conn;
				ResultSet rs = null;
				String file1="";
				String delFile="";//삭제될파일
				 String dftFilePath = request.getServletContext().getRealPath("/");
				 lk_upPath = dftFilePath+"academy/images";
				try{
					MultipartRequest multi=new MultipartRequest(
							request,
							lk_upPath,
							size,
							"utf-8",
							new DefaultFileRenamePolicy());
					int lo_s_code=0;
					for(int i =0; i<=15; i++){
					if(multi.getParameter("lee_s_code"+i) == null){}else{
					lo_s_code =  Integer.parseInt(multi.getParameter("lee_s_code"+i)); //지역(소)
					System.out.println("dlsdklfmsdfmewoim"+lo_s_code);
					}
					}
					
					int code = Integer.parseInt(multi.getParameter("lk_code")); //학원코드
					int lee_jiyug = Integer.parseInt(multi.getParameter("lee_jiyug")); //지역(대)
					String lee_sName = multi.getParameter("lee_sName"); //학원이름
					String lee_hakreview = multi.getParameter("lee_hakreview"); //학원소개
					String lee_sCurriculum = multi.getParameter("lee_sCurriculum"); //커리큘럼
					String lee_sFile = multi.getFilesystemName("lee_sFile"); //학원사진
					//위치주소?
					String lee_sAddress = multi.getParameter("lee_sAddress"); //학원 주소
					String mid = multi.getParameter("ip_mid"); // 관리자ID
					//등록일
					String lee_sEmail = multi.getParameter("lee_sEmail"); //학원이메일
					String lee_sPhone = multi.getParameter("lee_sPhone"); //학원 전화번호
					String lee_home = multi.getParameter("lk_home"); // 학원 홈페이지 주소
					
					String i1 = lee_hakreview.replace("\r\n", "<br>");
					String i2 = lee_sCurriculum.replace("\r\n", "<br>");
					
					//첨부파일 저리
					Enumeration e= multi.getFileNames();
					
					conn = new DBConn().getConn();
					
					
					sql = "update lk_academy set lk_bdoid=?, lk_scityid=?, lk_name=?, lk_txt=?, lk_cul=?, lk_logo=?, lk_map=?, lk_address=?, lk_email=?,lk_phone=?, lk_home=? "
							+ " where lk_code=?";
					ps = conn.prepareStatement(sql);
//													
					ps.setInt(1, lee_jiyug); 		//지역(대)
					ps.setInt(2, lo_s_code); 		//지역(소)
					ps.setString(3, lee_sName); 	//학원 이름
					ps.setString(4, i1); 			//학원 소개
					ps.setString(5, i2);			//커리큘럼F
					ps.setString(6, lee_sFile); 	//학원사진
					ps.setString(7, lee_sAddress); 	//위치주소?
					ps.setString(8, lee_sAddress);  //학원 주소
//												       등록일"
					ps.setString(9, lee_sEmail); 	//학원이메일
					ps.setString(10, lee_sPhone); 	//학원 전화번호
					ps.setString(11, lee_home); 	//학원 홈페이지 주소

					ps.setInt(12, code);			//학원 코드
					
					r = ps.executeUpdate();

					/**************************************************************************/
					for(int i=1; i<=8; i++){ //관련 코드목록 전부 삭제
						String lee_s_codel = multi.getParameter("lee_s_codel"+i);
						if(lee_s_codel != null){
							sql="delete from license where lk_code=?";
							ps = conn.prepareStatement(sql);
							ps.setInt(1, code);
							r = ps.executeUpdate();						}
					}
					
					for(int i=1; i<8; i++ ){ //  삭제후 다시입력
						String lee_s_codel =  multi.getParameter("lee_s_codel"+i);//자격증(소)
							
						if(lee_s_codel != null){
							int lee_jagiyg = Integer.parseInt(multi.getParameter("lee_jagiyg")); //자격증(대)
							int lee_s_lili = Integer.parseInt(lee_s_codel);
//							seq_lk_academy.currval
							sql="insert into license values(?,?,?)";
							ps = conn.prepareStatement(sql);
							ps.setInt(1, code);
							ps.setInt(2, lee_jagiyg);
							ps.setInt(3, lee_s_lili);
							r = ps.executeUpdate();
							
						}
					}
					/**************************************************************************/
					for(int i=1; i<=8; i++){ //관련 코드목록 전부 삭제
						String lee_s_codel = multi.getParameter("lee_s_code"+i);
						if(lee_s_codel != null){
							sql="delete from learning where lk_code=?";
							ps = conn.prepareStatement(sql);
							ps.setInt(1, code);
							r = ps.executeUpdate();						}
					}
					
					for(int i=1; i<8; i++ ){
						String lee_s_ha = multi.getParameter("lee_s_code"+i); //학문(소)
					
						if(lee_s_ha != null){
						int lee_l_ha = Integer.parseInt(multi.getParameter("lee_hakmoon")); //학문(대)
						int lee_s_code = Integer.parseInt(lee_s_ha);
						
						sql="insert into learning values(?,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, code);
						ps.setInt(2, lee_l_ha);
						ps.setInt(3, lee_s_code);
						r = ps.executeUpdate();
						}
					}
					/**************************************************************************/
					for(int i=1; i<=8; i++){ //관련 코드목록 전부 삭제
						String lee_tmig = multi.getParameter("lee_tmig"+i);
						if(lee_tmig != null){
							sql="delete from learning where lk_code=?";
							ps = conn.prepareStatement(sql);
							ps.setInt(1, code);
							r = ps.executeUpdate();						}
					}
					
					for(int i=0; i<=4; i++ ){
						String lee_tmig = multi.getFilesystemName("lee_tmig"+i); //강사 이미지
						String lee_ttext = multi.getParameter("lee_ttext"+i); //강사 내용
						
						String i3 = lee_ttext.replace("\r\n", "<br>");
						
						if(lee_tmig != null){
							
							System.out.println("lee_tmig = "+lee_tmig);					
							System.out.println("lee_ttext = "+i3);
							
							sql="insert into lk_teacher values(seq_lk_teacher.nextval,seq_lk_academy.currval,?,?) ";
							ps = conn.prepareStatement(sql);
							ps.setString(1, lee_tmig);
							ps.setString(2, lee_ttext);
							
							r = ps.executeUpdate();
						}
					}
					
				}catch(Exception ex){
					ex.printStackTrace();
				}finally{
					toString();
				}
				return r;
				}
			

	/* 1604 이현재 로직 끝@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	
	
	// setter , getter
	
	public int getCategory_val() {
		return category_val;
	}

	public void setCategory_val(int category_val) {
		this.category_val = category_val;
	}

	public String getLk_upPath() {
		return lk_upPath;
	}

	public void setLk_upPath(String lk_upPath) {
		this.lk_upPath = lk_upPath;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getLk_oldFile() {
		return lk_oldFile;
	}

	public void setLk_oldFile(String lk_oldFile) {
		this.lk_oldFile = lk_oldFile;
	}

	public int getTsearch() {
		return tsearch;
	}

	public void setTsearch(int tsearch) {
		this.tsearch = tsearch;
	}

	public int getListSize() {
		return listSize;
	}

	public void setListSize(int listSize) {
		this.listSize = listSize;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public int getTotSize() {
		return totSize;
	}

	public void setTotSize(int totSize) {
		this.totSize = totSize;
	}

	public int getTotPage() {
		return totPage;
	}

	public void setTotPage(int totPage) {
		this.totPage = totPage;
	}

	public int getTotBlock() {
		return totBlock;
	}

	public void setTotBlock(int totBlock) {
		this.totBlock = totBlock;
	}

	public int getNowBlock() {
		return nowBlock;
	}

	public void setNowBlock(int nowBlock) {
		this.nowBlock = nowBlock;
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getStartNo() {
		return startNo;
	}

	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}

	public int getEndNo() {
		return endNo;
	}

	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	

}
