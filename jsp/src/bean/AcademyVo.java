package bean;

import java.sql.Date;

public class AcademyVo {
	
	private int category_val;
	
	//lk_academy(학원)테이블
	private int lk_code=0;		 // 학원코드
	private int lk_scityid=0;	 // 시아이디
	private int lk_bdoid=0;		 // 도아이디
	private String lk_name="";	 // 학원이름
	private String lk_txt="";	 // 학원소개
	private String lk_cul="";	 // 커리큘럼
	private String lk_logo="";	 // 학원로코
	private String lk_map="";	 // 위치정보
	private String lk_address="";// 학원 주소
	private String lk_manage=""; // 관리자
	private Date lk_date;		 // 작성일
	private String lk_email="";	 // 학원 이메일
	private String lk_phone="";  // 학원 연락처
	private String lk_home=""; //학원 홈페이지 주소
	
	/*************************************************************************************/
	//lk_teacher(강사)테이블
	private String lk_timage=""; // 강사 사진
	private String lk_tname="";	 // 강사 이름
	private String lk_tcontent="";// 강사 소개
	private int lk_tserial=0;	 // 시리얼
	
	/*************************************************************************************/
	//지역 표시 분류
	//lk_do(지역대분류)
	private int l_doid=0;// 도 이름        
	//lk_city(지역소분류)
	private int s_cityid;// 시 이름
	//합쳤을시 명
	private String local_name=""; // 카테고리명
	
	
	//지역 대소 분류 이름
	private String lk_bdoname=""; // 지역 대분류 이름
	private String lk_scityname=""; // 지역 소분류 이름
	
	/*************************************************************************************/
	//학문 표시 분류
	//lk_blearning(학문대분류)
	private int l_hak;// 학문 대이름
	//lk_slearning(학문소분류)
	private int s_hak;// 학문 소이름
	//합쳤을시 명
	private String hak_name=""; //학문 카테고리명
	
	
	//학문 대소 분류 이름
	private String lk_bsname=""; // 학문 대분류 이름
	private String lk_ssname=""; // 학문 소분류 이름
	
	/*************************************************************************************/
	//자격증 표시 분류
	//lk_blearning(자격증 대분류)
	private int l_license;// 자격증 대이름
	//lk_slearning(자격증 소분류)
	private int s_license;// 자격증 소이름
	//합쳤을시 명
	private String license_name=""; //자격증 카테고리명
	
	
	//자격증 대소 분류 이름
	private String lk_bname=""; // 자격증 대분류 이름
	private String lk_sname=""; // 자격증 소분류 이름
	
	// 콤보박스 값 유지용
	private int l_loc;
	private int s_loc;
	private int l_lic;
	private int s_lic;
	
	private int l_code;
	private String learning_name;
	private int s_code;
	
	/*************************************************************************************/
	public int getLk_code() {
		return lk_code;
	}
	public void setLk_code(int lk_code) {
		this.lk_code = lk_code;
	}
	public int getLk_scityid() {
		return lk_scityid;
	}
	public void setLk_scityid(int lk_scityid) {
		this.lk_scityid = lk_scityid;
	}
	public int getLk_bdoid() {
		return lk_bdoid;
	}
	public void setLk_bdoid(int lk_bdoid) {
		this.lk_bdoid = lk_bdoid;
	}
	public String getLk_name() {
		return lk_name;
	}
	public void setLk_name(String lk_name) {
		this.lk_name = lk_name;
	}
	public String getLk_txt() {
		return lk_txt;
	}
	public void setLk_txt(String lk_txt) {
		this.lk_txt = lk_txt;
	}
	public String getLk_cul() {
		return lk_cul;
	}
	public void setLk_cul(String lk_cul) {
		this.lk_cul = lk_cul;
	}
	public String getLk_logo() {
		return lk_logo;
	}
	public void setLk_logo(String lk_logo) {
		this.lk_logo = lk_logo;
	}
	public String getLk_map() {
		return lk_map;
	}
	public void setLk_map(String lk_map) {
		this.lk_map = lk_map;
	}
	public String getLk_address() {
		return lk_address;
	}
	public void setLk_address(String lk_address) {
		this.lk_address = lk_address;
	}
	public String getLk_manage() {
		return lk_manage;
	}
	public void setLk_manage(String lk_manage) {
		this.lk_manage = lk_manage;
	}
	public Date getLk_date() {
		return lk_date;
	}
	public void setLk_date(Date lk_date) {
		this.lk_date = lk_date;
	}
	public String getLk_email() {
		return lk_email;
	}
	public void setLk_email(String lk_email) {
		this.lk_email = lk_email;
	}
	public String getLk_phone() {
		return lk_phone;
	}
	public void setLk_phone(String lk_phone) {
		this.lk_phone = lk_phone;
	}
	public String getLk_timage() {
		return lk_timage;
	}
	public void setLk_timage(String lk_timage) {
		this.lk_timage = lk_timage;
	}
	public String getLk_tname() {
		return lk_tname;
	}
	public void setLk_tname(String lk_tname) {
		this.lk_tname = lk_tname;
	}
	public String getLk_tcontent() {
		return lk_tcontent;
	}
	public void setLk_tcontent(String lk_tcontent) {
		this.lk_tcontent = lk_tcontent;
	}
	public int getLk_tserial() {
		return lk_tserial;
	}
	public void setLk_tserial(int lk_tserial) {
		this.lk_tserial = lk_tserial;
	}
	public int getL_doid() {
		return l_doid;
	}
	public void setL_doid(int l_doid) {
		this.l_doid = l_doid;
	}
	public int getS_cityid() {
		return s_cityid;
	}
	public void setS_cityid(int s_cityid) {
		this.s_cityid = s_cityid;
	}
	public String getLocal_name() {
		return local_name;
	}
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	public String getLk_bdoname() {
		return lk_bdoname;
	}
	public void setLk_bdoname(String lk_bdoname) {
		this.lk_bdoname = lk_bdoname;
	}
	public String getLk_scityname() {
		return lk_scityname;
	}
	public void setLk_scityname(String lk_scityname) {
		this.lk_scityname = lk_scityname;
	}
	public int getL_hak() {
		return l_hak;
	}
	public void setL_hak(int l_hak) {
		this.l_hak = l_hak;
	}
	public int getS_hak() {
		return s_hak;
	}
	public void setS_hak(int s_hak) {
		this.s_hak = s_hak;
	}
	public String getHak_name() {
		return hak_name;
	}
	public void setHak_name(String hak_name) {
		this.hak_name = hak_name;
	}
	public String getLk_bsname() {
		return lk_bsname;
	}
	public void setLk_bsname(String lk_bsname) {
		this.lk_bsname = lk_bsname;
	}
	public String getLk_ssname() {
		return lk_ssname;
	}
	public void setLk_ssname(String lk_ssname) {
		this.lk_ssname = lk_ssname;
	}
	public int getL_license() {
		return l_license;
	}
	public void setL_license(int l_license) {
		this.l_license = l_license;
	}
	public int getS_license() {
		return s_license;
	}
	public void setS_license(int s_license) {
		this.s_license = s_license;
	}
	public String getLicense_name() {
		return license_name;
	}
	public void setLicense_name(String license_name) {
		this.license_name = license_name;
	}
	public String getLk_bname() {
		return lk_bname;
	}
	public void setLk_bname(String lk_bname) {
		this.lk_bname = lk_bname;
	}
	public String getLk_sname() {
		return lk_sname;
	}
	public void setLk_sname(String lk_sname) {
		this.lk_sname = lk_sname;
	}
	public int getCategory_val() {
		return category_val;
	}
	public void setCategory_val(int category_val) {
		this.category_val = category_val;
	}
	public int getL_loc() {
		return l_loc;
	}
	public void setL_loc(int l_loc) {
		this.l_loc = l_loc;
	}
	public int getS_loc() {
		return s_loc;
	}
	public void setS_loc(int s_loc) {
		this.s_loc = s_loc;
	}
	public int getL_lic() {
		return l_lic;
	}
	public void setL_lic(int l_lic) {
		this.l_lic = l_lic;
	}
	public int getS_lic() {
		return s_lic;
	}
	public void setS_lic(int s_lic) {
		this.s_lic = s_lic;
	}
	public int getL_code() {
		return l_code;
	}
	public void setL_code(int l_code) {
		this.l_code = l_code;
	}
	public String getLearning_name() {
		return learning_name;
	}
	public void setLearning_name(String learning_name) {
		this.learning_name = learning_name;
	}
	public int getS_code() {
		return s_code;
	}
	public void setS_code(int s_code) {
		this.s_code = s_code;
	}
	public String getLk_home() {
		return lk_home;
	}
	public void setLk_home(String lk_home) {
		this.lk_home = lk_home;
	}
	
	
}