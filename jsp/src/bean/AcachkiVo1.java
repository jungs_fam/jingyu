package bean;

	public class AcachkiVo1 {
		
		private int l_code;// 학문 대이름
		private int s_code;// 학문 소이름
		private int l_cityid;// 지역 대이름
		private int s_cityid;// 지역 소이름
		private int l_license;// 자격증 대이름
		private int s_license;// 자격증 소이름

		private String learning_name=""; //학문 카테고리명	
		private String license_name=""; //자격증 카테고리명	
		private String local_name=""; //지역 카테고리명	
		
		private String lk_timage=""; //강사 이미지
		private String lk_tcontent=""; // 강사 텍스트
		
		
		
		
		public int getL_code() {
			return l_code;
		}
		public void setL_code(int l_code) {
			this.l_code = l_code;
		}
		public int getS_code() {
			return s_code;
		}
		public void setS_code(int s_code) {
			this.s_code = s_code;
		}
		public int getL_cityid() {
			return l_cityid;
		}
		public void setL_cityid(int l_cityid) {
			this.l_cityid = l_cityid;
		}
		public int getS_cityid() {
			return s_cityid;
		}
		public void setS_cityid(int s_cityid) {
			this.s_cityid = s_cityid;
		}
		public int getL_license() {
			return l_license;
		}
		public void setL_license(int l_license) {
			this.l_license = l_license;
		}
		public int getS_license() {
			return s_license;
		}
		public void setS_license(int s_license) {
			this.s_license = s_license;
		}
		public String getLearning_name() {
			return learning_name;
		}
		public void setLearning_name(String learning_name) {
			this.learning_name = learning_name;
		}
		public String getLicense_name() {
			return license_name;
		}
		public void setLicense_name(String license_name) {
			this.license_name = license_name;
		}
		public String getLocal_name() {
			return local_name;
		}
		public void setLocal_name(String local_name) {
			this.local_name = local_name;
		}
		public String getLk_timage() {
			return lk_timage;
		}
		public void setLk_timage(String lk_timage) {
			this.lk_timage = lk_timage;
		}
		public String getLk_tcontent() {
			return lk_tcontent;
		}
		public void setLk_tcontent(String lk_tcontent) {
			this.lk_tcontent = lk_tcontent;
		}
		
		
	
	}
