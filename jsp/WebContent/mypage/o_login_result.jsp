<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="bean.DBConn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login_result</title>
<link rel='stylesheet' href='mypage/o_all.css'/>
<script src='mypage/o_all.js'></script>


</head>
<body>

<%
try{
	
	String mid = request.getParameter("ip_mid");
	String pwd = request.getParameter("ip_pwd");
	String url="index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=";

	Connection conn = new DBConn().getConn();
	PreparedStatement ps = null;
	
	String sql = "select * from i_person where ip_mid = ? and ip_pwd = ?";

	ps = conn.prepareStatement(sql);
	ps.setString(1, mid);
	ps.setString(2, pwd);
	 
	ResultSet rs = ps.executeQuery();
	
	boolean isLogin = false;
	while(rs.next()){
		
		isLogin = true;
	}
	
	if(isLogin){
		
		%>
		
		<form name="frm" method = "post" action="#">
		<table id="bo1" width = "750" border = "1" cellspacing = "0" cellpadding = "3" align = "center">
		
		<tr>
    	  <td colspan = "2" height = "39" align = "center">
      		<font size = "+1"><h2><b>정말 탈퇴 하시겠습니까?</b></h2></font></td>
		</tr>
		
		<tr>
			<td id="col" colspan = "2" align = "center">
				<input type="button" value="회원탈퇴" onclick="o_delete('<%=mid%>')">
			</td>
		</tr>
		
		<input type='hidden' name='ip_mid' value="<%=mid%>"/>
		</table>
		
		</form>
		<% 
		/* session.setAttribute("ip_mid", mid);
		session.setAttribute("ip_pwd", pwd);
		 out.print("<script>");
		out.print("location.href='index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=o_delete_re.jsp'");
		out.print("</script>");  */
// 		response.sendRedirect("/web/final/index.jsp");
		
		
		
	}else{
		
		%><script>alert("로그인 실패"); history.go(-1);</script><%
	}
	
	}catch (Exception e){
	    out.print("DB 연동 실패");	
	}	
%>



</body>
</html>