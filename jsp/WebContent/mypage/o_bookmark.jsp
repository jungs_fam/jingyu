<%@page import="java.util.List"%>
<%@page import="bean.o_FinalVo"%>
<%@page import="bean.o_FinalDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='mypage/o_all.css'/>
</head>
<body>

<jsp:useBean id="dao" class="bean.o_FinalDao" scope="page"/>
<jsp:setProperty property="*" name="dao"/>
<jsp:useBean id="vo" class="bean.o_FinalVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<%
String search=dao.getSearch();
List<o_FinalVo> list = dao.list(vo);
String url="index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=";
String ip_mid =request.getParameter("ip_mid");
String nowPage =null;

%>
<table id="bo1" width = "750" border = "1" cellspacing = "0" cellpadding = "3" align = "center">
	<tr>
      <td colspan = "2" height = "39">
      <font size = "+1"><h2><b>즐겨찾기</b></h2></font></td>
	</tr>
</table>
 <div id='list'>
	<div id='search'>
		<form name='frm' method='post' 
			action='<%=url %>score/list.jsp'>
			<label>검색</label>
			<input type='text' name='search' value="<%=search%>"/>
			<input type='submit' value='검색'/>
		</form>
	</div> 

	
	
		<div class='row title'>
		
			<span>아이디</span>
			<span>제목</span>

		</div>
		
		<%
		for(o_FinalVo v : list){
			%>
		<div class='row item'>
			
			<span><%=v.getIp_mid() %> </span>
			<span><a href='#' onclick="#"><%= v.getIk_ip()%></a></span>
			
		</div>
		<% }%>
		<hr/>
	

	<div id='page_div'>
	
	<%
		// 맨처음, 이전 버튼
		if(dao.getNowBlock()>1){%>
			<input type='button' value='맨처음' 
					 onclick="moveList('<%=search%>', '<%=1%>')"/>
			<input type='button' value='이전' 
					 onclick="moveList('<%=search%>', '<%=dao.getNowPage()-1%>')"/>
		<%}
		
		  for(int i=dao.getStartPage() ; i<=dao.getEndPage() ; i++){ %>
			<input type='button' value='<%=i %>' 
					 onclick="moveList('<%=search%>', '<%=i%>')"/>
					
		<%}
		  
		// 맨끝, 다음 버튼
		if(dao.getNowBlock() < dao.getTotBlock()){%>
			<input type='button' value='다음' 
					 onclick="moveList('<%=search%>', '<%=dao.getEndPage()+1%>')"/>
			<input type='button' value='맨끝' 
					 onclick="moveList('<%=search%>', '<%=dao.getTotPage()%>')"/>
		<%}%>

	</div>

</div>


</body>
</html>