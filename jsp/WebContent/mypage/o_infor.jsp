<%@page import="bean.o_FinalVo"%>
<%@page import="bean.o_FinalDao"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='mypage/o_all.css'/>
<script>

	function btnInit(){
		var o_btnmain = document.getElementById('o_btnmain');
		
		
		o_btnmain.onclick=function(){
			location.href='index.jsp';
		}
	
	}

</script>
</head>
<body>

<jsp:useBean id="vo" class="bean.o_FinalVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<%
request.setCharacterEncoding("utf-8");

o_FinalDao dao = new o_FinalDao();
o_FinalVo v = dao.o_infor(vo);
String ip_mid = request.getParameter("ip_mid");
String ip_code = request.getParameter("ip_code");
/* out.print(ip_code); */

String url="index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=";




%>
<form name='frm' method='post' action='<%=url%>o_modify.jsp'>
<table id="bo1" width = "750" border = "1" cellspacing = "0" cellpadding = "3" align = "center">

<tr>
      <td colspan = "2" height = "39">
      <font size = "+1"><h2><b>회원 정보</b></h2></font></td>
</tr>

<tr>
      <td width = "200">ID</td>
      <td width = "600">
            <input type = "text" id="o_mid" readonly name='ip_mid' name = 'ip_mid' value = "<%=v.getIp_mid()%>">
            
      </td>
</tr>

<tr>
      <td width = "200">이름</td>
      <td width = "600">
           <input type = "text" id="o_irum" readonly name='ip_irum'name = 'ip_irum' value = "<%=v.getIp_irum()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">E-Mail</td>
      <td width = "600">
		<input type = "text" id="o_email" readonly name='ip_email'name = 'ip_email' value = "<%=v.getIp_email()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">연락처</td>
      <td width = "600">
		<input type = "text" id="o_phone" readonly name='ip_phone' name = 'ip_phone' value = "<%=v.getIp_phone()%>"><br/>
      </td>
</tr>
<tr>
      <td width = "200">우편번호</td>
      <td width = "600">
		<input type = "text" id="o_code" readonly name='ip_code'name = 'ip_code' value = "<%=v.getIp_code()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">주소</td>
      <td width = "600">
		<input type = "text" id="o_address" readonly name='ip_address' name='ip_address'value = "<%=v.getIp_address()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">사용가능한 포인트</td>
      <td width = "600">
		<input type = "text" id="o_point" readonly name='ip_point' name='ip_point' value = "<%=v.getIp_point()%>"><br/>
      </td>
</tr>

<tr>
	<td id="col" colspan = "2" align = "right">
		<input type="button" value="메인" id="o_btnmain">
		<input type="submit" value="회원정보 수정">
	</td>
</tr>
</table>
</form>




<script>
btnInit()

</script>
</body>
</html>