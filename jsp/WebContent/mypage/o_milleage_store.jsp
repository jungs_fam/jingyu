<%@page import="bean.o_FinalDao"%>
<%@page import="bean.MyPass"%>
<%@page import="com.sun.mail.util.MailSSLSocketFactory"%>
<%@page import="java.util.Properties"%>
<%@page import="javax.mail.Transport"%>
<%@page import="javax.mail.Session"%>
<%@page import="java.util.Date"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@page import="javax.mail.Message"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='mypage/o_all.css'/>
<script src='mypage/o_all.js'></script>
</head>
<body>
<jsp:useBean id="vo" class="bean.o_FinalVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<% 
String ip_mid =request.getParameter("ip_mid");

if(vo.getIp_point()>=vo.getIp_mil()){
o_FinalDao dao = new o_FinalDao();
int r = dao.o_modify2(vo);

String radio = request.getParameter("ip_mil");
String email = request.getParameter("ip_email");

String url="o_MyPage.jsp?inc=../java/o_menu_java.jsp&sub=../mypage1/";

if(r>0) {
	request.setCharacterEncoding("utf-8");

	 String sender = "loom24@naver.com";
	 String receiver = email;
	 String subject = "AcademyKorea에서 선물이 도착했습니다.";
	 String content = "문화상품권 "+radio+"원";
	 
	//  메일 전송
	 String host = "smtp.naver.com";
	 try{
//	 	 메일서버 설정 갭슐화
	  Properties prop = new Properties();
	  MailSSLSocketFactory msf = new MailSSLSocketFactory();
	  msf.setTrustAllHosts(true);
	  
	  
	  prop.put("mail.smtp.ssl.socketFactory", msf);
	  prop.put("mail.smtp.starttls.enable", "true");
	  prop.put("mail.smtp.ssl.enable", "true");
	  prop.put("mail.transport.protocol", "smtp");
	  prop.put("mail.smtp.port", "465");
	  prop.put("mail.smtp.auth", "true");
	  
	  prop.put("mail.smtp.user", receiver);
	  prop.put("mail.smtp.host", host);
	  
	//   사용하려는 메일 서버 권한자 지정

	 Session mailSession = Session.getInstance(prop, new MyPass());

	//   보내는 메세지 캡슐화
	 Message message = new MimeMessage(mailSession);
	 message.setFrom(new InternetAddress(sender));
	 message.setSubject(subject);
	 message.setSentDate(new Date());
	 message.setContent(content, "text/html;charset=utf-8");
	 message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));


	  Transport.send(message);

	 }catch(Exception ex){
		 out.print(ex.toString());
		
	 }
	
	out.print("사용완료");
	
}else {
	out.print("수정중 오류 발생");
}
}else{
	%> <script>alert("마일리지가 부족합니다"); history.go(-1);</script> <%
}
%>


<script>
osh_moveList('<%=ip_mid%>');

</script>
</body>
</html>