<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>menu_java</title>
<script src='mypage/o_all.js'></script>

<style>

.o_menubar{
background: rgb(255,228,196);
border:none;
border:0px;
margin:0px;
padding:0px;
font: 67.5% "Lucida Sans Unicode", "Bitstream Vera Sans", "Trebuchet Unicode MS", "Lucida Grande", Verdana, Helvetica, sans-serif;
font-size:16px;
}
.o_menubar ul{
background: rgb(255,228,196);
height:50px;
list-style:none;
margin:0;
padding:0;
}
.o_menubar li{
float:left;
padding:0px;
}
.o_menubar li a{
background: rgb(255,228,196);
color:#000000;
display:block;
font-weight:normal;
line-height:50px;
margin:0px;



padding:0px 25px;
text-align:center;
text-decoration:none;
}
.o_menubar li ul{
background: rgb(255,228,196);
display:none; /* 평상시에는 드랍메뉴가 안보이게 하기 */
height:auto;
padding:0px;
margin:0px;
border:0px;
position:absolute;
width:200px;
z-index:999;
/*top:1em;
/*left:0;*/
}
.o_menubar li a:hover{
background: rgb(255,140,000);
color:#FFFFFF;
text-decoration:none;
}
.o_menubar li li {
background: rgb(255,228,196);
display:block;
float:none;
margin:0px;
padding:0px;
width:200px;
}
.o_menubar li ul a{
display:block;
height:50px;
font-size:14px;
font-style:normal;
margin:0px;
padding:0px 10px 0px 15px;
text-align:left;
}
.o_menubar p{
clear:left;
}
#ddd{
margin-top:33px;
font-size:22px;


}



</style>
</head>
<body>
<%
String url="index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=";
String ip_mid = request.getParameter("ip_mid");

%>
<div class="o_menubar">
   <ul>
      <li><a href="#" id="ddd"><h3>마이페이지</h3></a></li><hr/>
      <li><a href="#" onclick ="o_mmit('<%=ip_mid %>')">나의정보</a></li>
      <li><a href='#' onclick ="o_mmmg('<%=ip_mid %>')">마일리지</a></li>
      <li><a href='#' onclick ="o_mmcp('<%=ip_mid %>')">쿠폰등록</a> </li>
      <li><a href="#" onclick ="o_mmbo('<%=ip_mid %>')">즐겨찾기</a></li>       
      <li><a href='#' onclick ="o_mmdl('<%=ip_mid %>')">회원탈퇴</a></li>
   </ul>
</div>


</body>
</html>