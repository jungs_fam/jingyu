<%@page import="bean.o_FinalVo"%>
<%@page import="bean.o_FinalDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='mypage/o_all.css'/>
<script src='mypage/o_all.js'></script>
</head>
<body>
<jsp:useBean id="vo" class="bean.o_FinalVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>
<%
request.setCharacterEncoding("utf-8");

o_FinalDao dao = new o_FinalDao();
o_FinalVo v = dao.o_infor(vo);
String ip_mid = request.getParameter("ip_mid");


String url="index.jsp?sub=mypage/o_MyPage.jsp?o_inc=o_menu_java.jsp&o_sub=";

%>

<form name='frm' method='post' action='<%=url %>o_modify_re.jsp' >

<table id="bo1" width = "750" border = "1" cellspacing = "0" cellpadding = "3" align = "center">

<tr>
      <td colspan = "2" height = "39">
      <font size = "+1"><h2><b>회원 정보 수정</b></h2></font></td>
</tr>

<tr>
      <td width = "200">ID</td>
      <td width = "600">
            <input type = "text" id="o_mid" readonly name='ip_mid' name = 'ip_mid' value = "<%=v.getIp_mid()%>">    
      </td>
</tr>

<tr>
      <td width = "200">암호</td>
      <td width = "600">
            <input type = "password" id="o_pwd" name = 'ip_pwd' value = "<%=v.getIp_pwd()%>">    
      </td>
</tr>

<tr>
      <td width = "200">암호확인</td>
      <td width = "600">
            <input type = "password" name='ip_pwdchk' onblur = 'pwdchk()' value = "">　    
            <input type="text" name="chk" value="비밀번호를 입력하세요" readonly="readonly" class="danger1" id="o_chk">
      </td>
</tr>

<tr>
      <td width = "200">이름</td>
      <td width = "600">
           <input type = "text" id="o_irum" readonly name='ip_irum'name = 'ip_irum' value = "<%=v.getIp_irum()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">E-Mail</td>
      <td width = "600">
		<input type = "text" size="40" name = 'ip_email' value = "<%=v.getIp_email()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">연락처</td>
      <td width = "600">
		<input type = "text"  name = 'ip_phone' value = "<%=v.getIp_phone()%>"><br/>
      </td>
</tr>
<tr>
      <td width = "200">우편번호</td>
      <td width = "600">
		<input type = "text"  name = 'ip_code' value = "<%=v.getIp_code()%>"><br/>
      </td>
</tr>

<tr>
      <td width = "200">주소</td>
      <td width = "600">
		<input type = "text" size="50" name="ip_address" value = "<%=v.getIp_address()%>"><br/>
      </td>
</tr>

<tr>
	<td id="col" colspan = "2" align = "right">
		<input type="button" value="수정완료" onclick="modifychk()">
	</td>
</tr>
</table>
</form>

<script>

</script>

</body>
</html>