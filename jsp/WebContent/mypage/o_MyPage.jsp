<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> 게시판 인덱스 </title>
<style type="text/css">
#o_main{
width: 960px;
overflow: auto;
}
#o_aside{
background: rgb(255,228,196);
border-right:1px solid #aaaaaa;
width: 180px;
height: 600px;
float: left;
}
#o_section{
width: 760px;
float: left;
}
</style>
</head>
<body>
<%
String o_inc  = "o_menu_java.jsp";
String o_sub = "o_infor.jsp";

if(request.getParameter("o_inc")!=null){
	o_inc = request.getParameter("o_inc");
}

if(request.getParameter("o_sub")!=null){
	o_sub = request.getParameter("o_sub");
}

%>
<div id="o_main">
<div id="o_aside">
<jsp:include page="<%=o_inc %>"></jsp:include>
</div>

<div id="o_section">
	<jsp:include page="<%=o_sub %>"></jsp:include>
		
</div>
</div>
</body>
</html>