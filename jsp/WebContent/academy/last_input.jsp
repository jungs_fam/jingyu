<%@page import="bean.AcademyVo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.lee_bt{
margin-left: 20px;
width: 80px; height: 30px;


  border-top: 1px solid #66e88f;
   background: #1946e8;
   background: -webkit-gradient(linear, left top, left bottom, from(#c74af0), to(#1946e8));
   background: -webkit-linear-gradient(top, #c74af0, #1946e8);
   background: -moz-linear-gradient(top, #c74af0, #1946e8);
   background: -ms-linear-gradient(top, #c74af0, #1946e8);
   background: -o-linear-gradient(top, #c74af0, #1946e8);
   padding: 8px 16px;
   -webkit-border-radius: 12px;
   -moz-border-radius: 12px;
   border-radius: 12px;
   -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
   -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
   box-shadow: rgba(0,0,0,1) 0 1px 0;
   text-shadow: rgba(0,0,0,.4) 0 1px 0;
   color: white;
   font-size: 14px;
   font-family: Georgia, serif;
   text-decoration: none;
   vertical-align: middle;
   }
   
.lee_bt:hover {
   border-top-color: #27bef5;
   background: #27bef5;
   color: #000000;
   }
.lee_bt:active {
   border-top-color: #ff002b;
   background: #ff002b;
   }
.black{display:inline-block;}
.lee_s1{
display: block;
}
.lee_t1{
text-align: center;
}
#lee_sName:FOCUS,#lee_sEmail:FOCUS,#lee_sPhone:FOCUS,#lee_sAddress:FOCUS,#lee_sCurriculum:FOCUS,.lee_ttext:FOCUS,#lk_home:FOCUS,#lee_hakreview:FOCUS{
  background-color: yellow;
}
.lee_btd{
margin-left: -50px;
margin-top: -20px;
}
.cen{text-align: center; display: block;}
.lee_ttext{
margin-left: 5px;
width: 600px;
height: 50px;
}
#lee_tmig{
margin-top: -20px;
}
</style>
<script>
var xhr; // 모든 곳에서 써야하기 때문에 var를 붙여서 준적역으로 생성
var rs;
var fileCnt = 0;//file, 삭제 button 태그의 아이디에 순번을 추가하기 위해

/* 자격증의 소분류를 보여주는 로직 *************************************************************************/
function go1(license){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
	    
		var url = 'last_LicenseSo.jsp';
		var ff = document.lee_frm;
		ff.lee_l_li.value = license;
		url += '?lee_l_li=' + license;
		xhr.onreadystatechange = myFunclicense;
		xhr.open('get', url, true);
		xhr.send();
}

// //수신된 결과를 result에 표시

function myFunclicense(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('lee_frmli2result');
		rs.innerHTML = xhr.responseText;
	}
}


/* 학문의 소분류를 보여주는 로직 *************************************************************************/
function go2(hak){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
	    
		var url = 'last_HakMoonSo.jsp';
		var ff = document.lee_frm;
		ff.lee_l_code.value = hak;
		url += '?lee_l_code=' + hak;
		xhr.onreadystatechange = myFunchak;
		xhr.open('get', url, true);
		xhr.send();
}

// //수신된 결과를 result에 표시

function myFunchak(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('lee_frmhak2result');
		rs.innerHTML = xhr.responseText;
	}
}


/* 지역의 소분류를 보여주는 로직 *************************************************************************/
function go3(local){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
	    
		var url = 'last_LocalSo.jsp';
		var ff = document.lee_frm;
		ff.lee_l_lo.value = local;
		url += '?lee_l_lo=' + local;
		xhr.onreadystatechange = myFunclocal;
		xhr.open('get', url, true);
		xhr.send();
}
////수신된 결과를 result에 표시
function myFunclocal(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('lee_frmlo2result');
		rs.innerHTML = xhr.responseText;
	}
}


/* 강사이미지 추가 삭제 로직 *************************************************************************/
function fileatt_init(){
	if(window.ActiveXObject){
		xhr = new ActiveXOject("Msxml2.XMLHTTP");
	}else{
		xhr = new XMLHttpRequest();
	}
	
	document.getElementById("lee_tmig").onclick = appendChild;
	document.getElementById("lee_btnDelete0").onclick = delFunc;
}
//file 태그를 추가하는 함수
function appendChild(ev) {
var ff = document.lee_frm;
fileCnt++;

//추가하려는 새로운 태그
var br = document.createElement('br');
var display = document.createElement('block');
var element = document.createElement('input');
var delBtn  = document.createElement('input');
var text = document.createElement("textarea");
var hr = document.createElement('hr');

// file 태그의 속성 지정
element.setAttribute('type', 'file');
element.setAttribute('name', 'lee_tmig'+ fileCnt);
element.setAttribute('class', 'lee_tmig');

text.setAttribute('type', 'text');
text.setAttribute('name', 'lee_ttext'+ fileCnt);
text.setAttribute('class', 'lee_ttext');
text.setAttribute('placeholder', '강사이름 및 정보 작성!');

element.onclick = appendChild;

ff.appendChild(element); // 파일버튼 누를시 바로 파일을 추가 로식 <br/>적용
ff.appendChild(br);




	
	
	//삭제버튼의 속성지정
	delBtn.setAttribute('type', 'button');
	delBtn.setAttribute('value', 'X');
	delBtn.setAttribute('id', 'lee_btnDelete' + fileCnt);
	delBtn.setAttribute('class', 'lee_btd');
	delBtn.onclick = delFunc;
	
	ff.appendChild(delBtn); // 파일버튼 클릭시 바로 삭제버튼 추가 및 텍스트도 추가 <br/>적용
	ff.appendChild(text);
	ff.appendChild(display);
	ff.appendChild(br);
	ff.appendChild(hr);
}

//X버튼의 좌측 file 태그와 우측 br 태그 삭제
function delFunc(ev){
var element = ev.srcElement;//이벤트가 발생한 요소(태그)
var prev = element.previousSibling;
var next = element.nextSibling;

element.parentNode.removeChild(prev);
element.parentNode.removeChild(next);
element.parentNode.removeChild(element);
}


function  cancel() { //취소버튼
	ff = window.close();
	     ff.submit();
}
 
function sub() {
	var ff = document.lee_frm;
	ff.submit();
}
 
</script>
</head>
<%
String im_id =(String)session.getAttribute("im_id");//로그인 아이디 값 보내기
%>
<body style="width: 940px;">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<jsp:useBean id="dao" class="bean.AcademyDao" scope="page"/>
<jsp:setProperty property="*" name="dao"/>

<h1 style="text-align: center; padding-bottom: 50px">학원 정보 입력</h1>

<form name="lee_frm" action="last_input_result.jsp" method="post"  enctype="multipart/form-data"   >
<div  style="margin-bottom:-80px;">
<label class='lee_s1'>학원 이름</label>
<input type='text' name='lee_sName' id='lee_sName'/>

<label style="float: right; margin-right: 10px; margin-top:-70px;">학원 사진<br/><input type="file" name='lee_sFile' id='lee_sFile' /></label><br/>

<label class='lee_s1'>학원 전화번호</label>
<input type='text' name='lee_sPhone' id='lee_sPhone' /><br/>
  
<label class='lee_s1'>학원 이메일</label>
<input type='text' name='lee_sEmail' id='lee_sEmail' /><br/>

<label class='lee_s1'>학원 주소</label><br/><br/>
<textarea rows="1" cols="30" style="margin-top: -40px;" name='lee_sAddress' id='lee_sAddress'></textarea><br/>
<label class='lee_s1' style="margin-top: -10px;">학원 홈페이지 주소</label>
<input type='text' name='lk_home' id='lk_home' size="30" />
</div>

<label style="float: right; margin-top: -140px; margin-right:5px;">학원소개<br/>
<textarea rows="13" cols="85" name='lee_hakreview' id='lee_hakreview'></textarea></label><br/>
<br/><br/><br/>

<label>커리큘럼</label><br/>
<textarea rows="13" cols="130" name='lee_sCurriculum' id='lee_sCurriculum'></textarea><br/>


<hr/>
<!-- 자격증 관련 블록(대) ****************************************** -->
<label class='cen'>※자격증※</label><br/><br/>
<%

List<AcademyVo> list;

list = dao.license();
for(int i=0; i<list.size(); i++){
	AcademyVo v = list.get(i);%>
	
	<label><input type="radio" name='lee_jagiyg' class='black' onclick='go1(<%=v.getL_license() %>)' /><%=v.getLicense_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}%>

<%}%><br/><br/><br/>
<hr/>
<!-- 자격증 관련 블록(소) ****************************************** -->
<label class='cen'>※관련 자격증※</label>
<div id='lee_frmli2result'><input type="hidden" name='lee_s_li' /></div><br/><br/><!-- 소분류 값 뿌려주기 -->

<hr/>
<!-- 학문 관련 블록(대) ****************************************** -->
<label class='cen'>※학문※</label><br/><br/>
<%
list = dao.study();
for(int i=0; i<list.size(); i++){
	AcademyVo v = list.get(i);%>
	
	<label><input type="radio" name='lee_hakmoon' class='black' onclick="go2(<%=v.getL_code() %>)" /><%=v.getLearning_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}%>
<%}%>
<hr/>
<!-- 학문 관련 블록(소) ****************************************** -->
<label class='cen'>※관련 학문※</label><br/>
<div id='lee_frmhak2result' ><input type="hidden" name='lee_s_code' /></div><br/><Br/><!-- 소분류 값 뿌려주기 -->



<hr/>
<!-- 지역 관련 블록(대) ****************************************** -->
<label class='cen'>※지역(도/시)※</label><br/><br/>
<%
list = dao.Local();
for(int i=0; i<list.size(); i++){
	AcademyVo v = list.get(i);%>
	<label><input type="radio" name='lee_jiyug' class='black' onclick="go3(<%=v.getL_doid() %>)" /><%=v.getLocal_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}%>
<%}%>
<hr/>
<!-- 지역 관련 블록(소) ****************************************** -->
<label class='cen'>※지역(동/구)※</label><br/>
<div id='lee_frmlo2result'><input type="hidden" name='lee_s_lo' /></div><!-- 소분류 값 뿌려주기 -->


<hr/>




<div class='lee_bor1'>
<label class='cen'>※강사이미지※</label><br/><br/>
<input type='file' id='lee_tmig' name='lee_tmig0' class='lee_tmig' /><input type='button' value='X' id='lee_btnDelete0' class='lee_btd'/><textarea name='lee_ttext0' class='lee_ttext' placeholder='강사이름 및 정보 작성!'></textarea>

</div><hr/>



<input type="hidden" name='im_id' value="<%=im_id %>" /> <!-- 세션값 로그인Id값 받아서 보냄 -->
<input type="hidden" name='lee_l_li' /> <!-- 자격증 대분류값 받아서 소분류 뿌리기전 보내기 -->
<input type="hidden" name='lee_l_code' /> <!-- 학문 대분류값 받아서 소분류 뿌리기전 보내기 -->
<input type="hidden" name='lee_l_lo' /> <!-- 지역 대분류값 받아서 소분류 뿌리기전 보내기 -->
</form>


<div style="text-align: center; margin-top:50px;">
<input type="button" class='lee_bt' onclick="sub()" value="등록" />
<input type="button" class='lee_bt' onclick="cancel()" value="취소"/>
</div>

<script>
 fileatt_init();
</script>

</body>
</html>