<%@page import="bean.AcachkiVo1"%>
<%@page import="bean.AcademyDao"%>
<%@page import="bean.AcademyVo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.lee_bt{
margin-left: 20px;
width: 80px; height: 30px;


  border-top: 1px solid #66e88f;
   background: #1946e8;
   background: -webkit-gradient(linear, left top, left bottom, from(#c74af0), to(#1946e8));
   background: -webkit-linear-gradient(top, #c74af0, #1946e8);
   background: -moz-linear-gradient(top, #c74af0, #1946e8);
   background: -ms-linear-gradient(top, #c74af0, #1946e8);
   background: -o-linear-gradient(top, #c74af0, #1946e8);
   padding: 8px 16px;
   -webkit-border-radius: 12px;
   -moz-border-radius: 12px;
   border-radius: 12px;
   -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
   -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
   box-shadow: rgba(0,0,0,1) 0 1px 0;
   text-shadow: rgba(0,0,0,.4) 0 1px 0;
   color: white;
   font-size: 14px;
   font-family: Georgia, serif;
   text-decoration: none;
   vertical-align: middle;
   }
   
.lee_bt:hover {
   border-top-color: #27bef5;
   background: #27bef5;
   color: #000000;
   }
.lee_bt:active {
   border-top-color: #ff002b;
   background: #ff002b;
   }
.black{display:inline-block;}
.lee_s1{
display: block;
}
.lee_t1{
text-align: center;
}
#lee_sName:FOCUS,#lee_sEmail:FOCUS,#lee_sPhone:FOCUS,#lee_sAddress:FOCUS,#lee_sCurriculum:FOCUS,.lee_ttext:FOCUS,#lk_home:FOCUS,#lee_hakreview:FOCUS{
  background-color: yellow;
}
.lee_btd{
margin-left: -50px;
margin-top: -20px;
}
.cen{text-align: center; display: block;}
.lee_ttext{
margin-left: 5px;
width: 600px;
height: 50px;
}
#lee_tmig{
margin-top: -20px;
}
label input{
display: inline;
}
<%
 for(int i=1; i<25; i++){%>
#h<%=i%> ~ #t<%=i%>{display: none;} /* 학문 담긴 값을 숨김 */	
#h<%=i%>:checked ~ #t<%=i%>{display: inline;}  /* 학문 누를시 숨긴값 표현 */

#l<%=i%> ~ #tt<%=i%>{display: none;} /* 자격증 담긴 값을 숨김 */	
#l<%=i%>:checked ~ #tt<%=i%>{display: inline;}  /* 자격증 누를시 숨긴값 표현 */

#c<%=i%> ~ #cs<%=i%>{display: none;} /* 지역 담긴 값을 숨김 */	
#c<%=i%>:checked ~ #cs<%=i%>{display: inline;}  /* 지역 누를시 숨긴값 표현 */
<%}%>
</style>
<script>
var xhr; // 모든 곳에서 써야하기 때문에 var를 붙여서 준적역으로 생성
var fileCnt = 0;//file, 삭제 button 태그의 아이디에 순번을 추가하기 위해
/* 강사이미지 추가 삭제 로직 *************************************************************************/
function fileatt_init(){
	if(window.ActiveXObject){
		xhr = new ActiveXOject("Msxml2.XMLHTTP");
	}else{
		xhr = new XMLHttpRequest();
	}
	
	document.getElementById("lee_tmig").onclick = appendChild;
	document.getElementById("lee_btnDelete0").onclick = delFunc;
}
//file 태그를 추가하는 함수
function appendChild(ev) {
var ff = document.lee_frm;
fileCnt++;

//추가하려는 새로운 태그
var br = document.createElement('br');
var display = document.createElement('block');
var element = document.createElement('input');
var delBtn  = document.createElement('input');
var text = document.createElement("textarea");
var hr = document.createElement('hr');

// file 태그의 속성 지정
element.setAttribute('type', 'file');
element.setAttribute('name', 'lee_tmig'+ fileCnt);
element.setAttribute('class', 'lee_tmig');

text.setAttribute('type', 'textarea');
text.setAttribute('name', 'lee_ttext'+ fileCnt);
text.setAttribute('class', 'lee_ttext');
text.setAttribute('placeholder', '강사이름 및 정보 작성!');

element.onclick = appendChild;

ff.appendChild(element); // 파일버튼 누를시 바로 파일을 추가 로식 <br/>적용
ff.appendChild(br);

	
	//삭제버튼의 속성지정
	delBtn.setAttribute('type', 'button');
	delBtn.setAttribute('value', 'X');
	delBtn.setAttribute('id', 'lee_btnDelete' + fileCnt);
	delBtn.setAttribute('class', 'lee_btd');
	delBtn.onclick = delFunc;
	
	ff.appendChild(delBtn); // 파일버튼 클릭시 바로 삭제버튼 추가 및 텍스트도 추가 <br/>적용
	ff.appendChild(text);
	ff.appendChild(display);
	ff.appendChild(br);
	ff.appendChild(hr);
}

//X버튼의 좌측 file 태그와 우측 br 태그 삭제
function delFunc(ev){
var element = ev.srcElement;//이벤트가 발생한 요소(태그)
var prev = element.previousSibling;
var next = element.nextSibling;

element.parentNode.removeChild(prev);
element.parentNode.removeChild(next);
element.parentNode.removeChild(element);
}

function  cancel() { //취소버튼
	 var ff = window.close();
	     ff.submit();
}
function sub() { // 서브밋 버튼
	ff = document.lee_frm;
	ff.submit();
}

</script>
</head>
<body style="width: 940px;">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<jsp:useBean id="vo" class="bean.AcademyVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>
<%
int code = Integer.parseInt(request.getParameter("lk_code"));
// int code = 1019;
String manager = request.getParameter("mid");
// String manager = "서울특별시 서초구 강남대로 419"; //값은 간다.....
AcademyDao dao = new AcademyDao();
AcademyVo rvo = dao.view(code, manager);

AcademyVo v=null;
List<AcademyVo> list;


List<AcachkiVo1> rvo1;

%>
<%=code %> <%=manager %>
<h1 style="text-align: center; padding-bottom: 50px">학원 정보 수정</h1><br/>

<form name="lee_frm" action="last_update_result.jsp" method="post"  enctype="multipart/form-data"   >
<input type="hidden" name='lk_code' value="<%=rvo.getLk_code() %>" />  <!-- 학원 코드  -->
<div  style="margin-bottom:-80px;">
<label class='lee_s1'>학원 이름</label>
<input type='text' name='lee_sName' id='lee_sName' value="<%=rvo.getLk_name() %>" />

<label style="float: right; margin-right: 10px; margin-top:-70px;">학원 사진<br/><input type="file" name='lee_sFile' id='lee_sFile' /></label><br/>

<label class='lee_s1'>학원 전화번호</label>
<input type='text' name='lee_sPhone' id='lee_sPhone' value="<%=rvo.getLk_phone() %>" /><br/>
  
<label class='lee_s1'>학원 이메일</label>
<input type='text' name='lee_sEmail' id='lee_sEmail' value="<%=rvo.getLk_email() %>" /><br/>

<label class='lee_s1'>학원 주소</label><br/><br/>
<textarea rows="1" cols="30" style="margin-top: -40px;" name='lee_sAddress' id='lee_sAddress' ><%=rvo.getLk_address() %></textarea><br/>
<label class='lee_s1' style="margin-top: -10px;">학원 홈페이지 주소</label>
<input type='text' name='lk_home' id='lk_home' size="30" value="<%=rvo.getLk_home() %>" />
</div>

<label style="float: right; margin-top: -140px; margin-right:5px;">학원소개<br/>
<textarea rows="13" cols="85" name='lee_hakreview' id='lee_hakreview' ><%=rvo.getLk_txt() %></textarea></label><br/>
<br/><br/><br/>

<label>커리큘럼</label><br/>
<textarea rows="13" cols="130" name='lee_sCurriculum' id='lee_sCurriculum'><%=rvo.getLk_cul() %></textarea><br/>

<hr/>
<!-- 자격증 관련 블록(대) ****************************************** -->
<label class='cen'>※자격증※</label><br/><br/>
<%

list = dao.license();


for(int i=0; i<list.size(); i++){
	v = list.get(i);%>
	
	<input type="radio" id='l<%=v.getL_license() %>' name='lee_jagiyg' class='black' value="<%=v.getL_license() %>" />
	<label for='l<%=v.getL_license() %>'><%=v.getLicense_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}%>
<%}%><br/><br/><br/>

<%
rvo1 = dao.view0(code);
for(int i=0; i<rvo1.size(); i++){ // 자격증 대의 값을 가져옴
	AcachkiVo1 v1 = rvo1.get(i);%>
<script>
licenseChlick();
function licenseChlick() { //값을 비교하여 체크
	 var i;
	 var rname = document.getElementsByName("lee_jagiyg");
	 for(i=0; i<16; i++){
		 if(rname[i].value == <%=v1.getL_code()%>){ 
			 rname[i].checked=true;
		 }
	 }
}
</script>
<%}%>
<hr/>
<!-- 자격증 관련 블록(소) ****************************************** -->
<label class='cen'>※관련 자격증※</label>
<%
int sizes = 0;
for(int ii=1; ii<=list.size(); ii++){rvo1 = dao.mody2(ii);
sizes = rvo1.size();
for(int t=0; t<rvo1.size(); t++){
	AcachkiVo1 v1 = rvo1.get(t);
	int i =v1.getS_license();
if(i == 0 ){}else{%>

	<label id='tt<%=ii %>'>
	<input type="radio" name='lee_s_codel<%=ii %>'  value="<%=v1.getS_license() %>" /><%=v1.getLicense_name() %>
	<%if(i%7 == 6){out.print("<br/>");}%></label>
<%}}}
rvo1 = dao.view0(code);
for(int i=0; i<rvo1.size(); i++){  // 자격증 소의 값을 가져옴
	AcachkiVo1 v1 = rvo1.get(i);%>
<script>
license_S_Chlick();
function license_S_Chlick(){ //값을 비교하여 체크
	var cname = document.getElementsByName("lee_s_codel<%=v1.getL_code()%>");
	for(var i=0; i<<%=sizes%>; i++){
		if(cname[i].value == <%=v1.getS_code()%>){
			cname[i].checked=true;
		}
	}
}
</script>
<%}%>
<hr/>
<!-- 학문 관련 블록(대) ****************************************** -->
<label class='cen'>※학문※</label><br/><br/>
<%
list = dao.study();

for(int i=0; i<list.size(); i++){
	v = list.get(i);%>
	
	<input type="radio" id='h<%=v.getL_code() %>' name='lee_hakmoon' class='black' value="<%=v.getL_code() %>"/>
	<label for='h<%=v.getL_code() %>'><%=v.getLearning_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}
}

rvo1 = dao.view1(code);
for(int i=0; i<rvo1.size(); i++){ // 학문 대의 값을 가져옴
	AcachkiVo1 aca1 = rvo1.get(i);%>
<script>
LearningChlick();
function LearningChlick(){  //값을 비교하여 체크
var i;
var h2 = document.getElementsByName("lee_hakmoon");
for(i=1; i<=11; i++){
	if(h2[i].value == <%=aca1.getL_code()%> ){
		h2[i].checked=true;
	}
	
}
}
</script>
<%}%>

<hr/>
<!-- 학문 관련 블록(소) ****************************************** -->
<label class='cen'>※관련 학문※</label><br/>
<%

int sizeS = 0;
for(int ii=1; ii<=list.size(); ii++){rvo1 = dao.mody1(ii);
sizeS = rvo1.size();
for(int t=0; t<rvo1.size(); t++){ 
AcachkiVo1 v1 = rvo1.get(t);
int vv =v1.getS_code();
if(vv == 0){}else{
%>
	<label id='t<%=ii %>'>
	<input type="radio" name='lee_s_codeh<%=ii%>'  value="<%=v1.getS_code() %>" /><%=v1.getLearning_name() %>
	<%if(ii%7 == 6){out.print("<br/>");}%></label>
<%}}}

rvo1 = dao.view1(code);
for(int i=0; i<rvo1.size(); i++){
	AcachkiVo1 aca2 = rvo1.get(i);%>
<script>
Learning_S_Chlick();
function Learning_S_Chlick(){
	var cname = document.getElementsByName("lee_s_codeh<%=aca2.getL_code()%>");
	for(var i=0; i<<%=sizeS%>; i++){
		if(cname[i].value == <%=aca2.getS_code()%>){
			cname[i].checked=true;
		}
}
}
</script>
<%}%>

<hr/>
<!-- 지역 관련 블록(대) ****************************************** -->
<label class='cen'>※지역(도/시)※</label><br/><br/>
<%
list = dao.Local(); 
for(int i=0; i<list.size(); i++){
	v = list.get(i);%>
	
	<input type="radio" id='c<%=v.getL_doid() %>' name='lee_jiyug' class='black' value="<%=v.getL_doid() %>" />
	<label for='c<%=v.getL_doid() %>'><%=v.getLocal_name() %></label>
	<%if(i%7 == 6){out.print("<br/>");}%>
<%}%>
	

<script>
LocalChlick();
function LocalChlick(){ //값을 비교하여 체크
	var i;
	var v2 = document.getElementsByName("lee_jiyug");
	for(i=0; i<9; i++){
		if(v2[i].value == <%=rvo.getLk_bdoid()%>){
			v2[i].checked=true;
		}
	}
}

</script>
<hr/>
<!-- 지역 관련 블록(소) ****************************************** -->
<label class='cen'>※지역(동/구)※</label><br/>
<%
for(int ii=1; ii<=list.size(); ii++){rvo1 = dao.Locals(ii); 
for(int l=0; l<rvo1.size(); l++){
AcachkiVo1 loc = rvo1.get(l);
int i =loc.getS_cityid();
if(i == 0){}else{
%>

	<label id='cs<%=ii %>'>
	<input type="radio" name='lee_s_code<%=ii %>' value="<%=loc.getS_cityid() %>" /><%=loc.getLocal_name() %>
	<%if(i%10 == 9){out.print("<br/>");}%></label>
<%}}}%>
<script>

Local_S_Chlick();
function Local_S_Chlick() {
	var lname = document.getElementsByName("lee_s_code<%=rvo.getLk_bdoid() %>");
	for(var i=0; i<<%=rvo1.size()%>; i++){
		if(lname[i].value == <%=rvo.getLk_scityid() %>){
			lname[i].checked=true;
		
	}
}
}
</script>
<hr/>
<div class='lee_bor1'>
<label class='cen'>※강사이미지※</label><br/><br/>
<%
rvo1 = dao.view2(code);
for(int i=0; i<rvo1.size(); i++){ // 저장된값을 불러온다
	AcachkiVo1 v2 = rvo1.get(i);
	%>

<input type='file' id='lee_tmig' name='lee_tmig0' class='lee_tmig' value="<%=v2.getLk_timage() %>" />
<input type='button' value='X' id='lee_btnDelete0' class='lee_btd'/>
<textarea name='lee_ttext0' class='lee_ttext'><%=v2.getLk_tcontent() %></textarea>

<%}%>
</div><hr/>

</form>

<div style="text-align: center; margin-top:50px;">
<input type="button" class='lee_bt' onclick="sub()" value="수정" />
<input type="button" class='lee_bt' onclick="cancel()" value="취소"/>

</div>


<script>
fileatt_init();
</script>

</body>
</html>