<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
a{
    font-weight: bold;
    font-size: 12px;
    color: #999;
    text-decoration: none;
}
</style>
<link rel="stylesheet" type="text/css" href="./css/font.css" />
</head>
<body>
	<div id="footer">
	<div class="family_site">
		<div class="inner_wrap">
			<a href="http://www.japansisa.com/" target="_blank"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;시사일본어학원</a>
			<a href="http://book.chinasisa.com/" target="_blank">&nbsp;|&nbsp; 시사북스 (주)시사중국어사</a>
			<a href="http://online.chinasisa.com/" target="_blank">&nbsp;|&nbsp; One on One 온라인 시사중국어학원</a>
			<a href="http://uhak.chinasisa.com/" target="_blank">&nbsp;|&nbsp; 시사중국유학본부</a>
			<a href="http://www.outedu.com/" target="_blank">&nbsp;|&nbsp; 시사출강교육</a>
			<a href="http://www.chinacpt.co.kr/" target="_blank">&nbsp;|&nbsp; 중국어실용능력시험 CPT</a>
		</div>
	</div>

</body>
</html>