<%@page import="bean.Lk_InterViewVo"%>
<%@page import="bean.Lk_InterViewDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>

body{
  width:1200px; /* body가 가운데 정렬할려면 width가 지정 되어야 한다? */
  margin: 0 auto;
}

  #InterView{
    border: 1px solid black;
    width:1200px;
    margin: 0 auto;
  }
  
  #head{
   border: 1px solid black;
    width:1200px;
    height:100px;
  }
  
  .headName{  
   border: 1px solid black;
   width:750px;
   height:60px;
   display:inline-block;
   float:left;
   margin-top:20px;
   margin-left:190px;
  }
  
  .ViewBtn{
   border: 1px solid black;   
   display:inline-block;
   float:left;
   margin-top:63px;
   margin-left:67px;
  }
  
 #box{
 
  width:1150px;
  height:650px;
  margin-top:10px;
  margin-left:30px;
  display:inline-block;
 
 }
  
  .StudentJpg{
   
   width:500px;
   height:600px;
   float:left;
   margin-top:15px;
   margin-left:50px;
  }
  
  .stimg1{
   border: 1px solid black;
   width:500px;
   height:280px;
   margin-top:10px;
  }
  
  .stimg2{
   border: 1px solid black;
   width:500px;
   height:280px;
   margin-top:10px;
  }
  
  .StudentCon{
   border: 1px solid black;
   width:500px;
   height:590px;
  float:left;
   margin-top:15px;
   margin-left:55px;
  }
  
  #ViewReBtn{
   padding:10px;
   margin-top:15px;   
   margin-left:550px;
  }
  
</style>
 
<script>
  function InterViewInit(){
	  var InterViewModify = document.getElementById("InterViewModify");
	  var InterViewDelete = document.getElementById("InterViewDelete");
	  var InterViewMove = document.getElementById("InterViewMove");
	  var ViewMove = document.getElementById("ViewMove");
	  
	  InterViewModify.onclick =function(){
		  location.href="interViewModify.jsp";
	  }
	  
	  InterViewDelete.onclick=function(){
		  location.href="interViewDelete.jsp"; 		  
	  }
	  
	  InterViewMove.onclick=function(){
		  location.href="interViewList.jsp"; 		  
	  }
	  
	  ViewMove.onclick=function(){
		  location.href=""; 		  
	  }
  }
</script>
</head>
<body>

<jsp:useBean id="vo" class="bean.Lk_InterViewVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<h1>인터뷰 상세보기</h1>
<div id="InterView">

<%
String url = "../academy/images/";
String code = request.getParameter("lk_code");
		Lk_InterViewDao dao = new Lk_InterViewDao();
        Lk_InterViewVo v = dao.view(vo.getSerial());
		
%>
	<%=code %>
  <div id="head">
     <div class="headName"><%=v.getLk_subject() %></div>
     <div class="ViewBtn">
       <input type="button" id="InterViewMove" value="인터뷰List"/>
       <input type="button" id="ViewMove" value="학원정보View"/>
     </div>
  </div>

<div id = "box">
  <div class="StudentJpg">
    <div class="stimg1">
      <img src="<%=url %><%=v.getFile1() %>" style="width:500px; hieght:280px;"/>
    </div>
    <div class="stimg2">
      <img src="<%=url %><%=v.getFile2() %>" style="width:500px; hieght:280px;"/>
    </div>
  </div>
  
  <div class="StudentCon">
      <%=v.getLk_content()%>
  </div>
</div>
  <div id="ViewReBtn">
    <input type="button" id="InterViewModify" value="수정"/>
    <input type="button" id="InterViewDelete" value="삭제"/>
  </div>
</div>

</body>
<script>InterViewInit('<%=vo.getSearch()%>','<%=vo.getNowPage()%>',
		   '<%=vo.getSerial()%>','<%=v.getLk_manager()%>');</script>
</html>









