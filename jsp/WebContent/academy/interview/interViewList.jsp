<%@page import="bean.Lk_InterViewVo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
 body{
   width : 1200px;
   margin: 0 auto;
 }

 #section{
  border: 1px solid black;
  margin: 0 auto;
  }

#search{
 text-align:right; 
 overflow:hidden;
 margin-top:10px;
 
}

#search .cdfrm{
  margin-right:10px;
}

#search #btnView{
 float:left;
 margin-left:10px;
}

#interviewList{
  border: 1px solid gray;  
  width:1198px; 
  margin-top:7px;
 
}

 .list1{ 
 border: 1px solid gray;
  display:inline-block;
  width:1140px;
  height:260px;
  margin-top:20px;
   margin-left:30px;
 }
 
 .listimg{ 
  display:inline-block;
  width:600px;
  height:250px;
  margin-left:8px;
  margin-top:3px;
 }
 
 .subcontent {
   display:inline-block;
  width:500px;
  height:250px;
  margin-left:8px;
  margin-top:3px;
 }
 
 .listsub{
  border:1px solid gray;
  width:500px;
  height:80px;
  text-overflow:ellipsis;
  overflow:hidden;
  white-space:nowrap;
 }
 
 .listcon{
  border:1px solid gray;
  width:500px;
  height:166px;
  text-overflow:ellipsis;
  overflow:hidden;
  white-space:nowrap;
 }
  
 #page{
  border: 1px solid gray;
  height:70px;
   
 }
 
 #page_div{
   margin-top:20px;
   margin-left:570px;
 }

</style>

<script> 
	
  function interview(lk_code){  	 
	 var ff = document.lk_interFrm;
	  ff.lk_code.value=lk_code;	 	  
	  document.getElementById("btnView").onclick = function(){
		  alert("adas");
		  ff.action = './academy/view.jsp';
		  ff.submit();
	  } 
	  
	
  }
   
  function moveList(search, nowPage){
    
	var ff = document.lk_interFrm;	
	ff.search.value=search;
	ff.nowPage.value= nowPage;	
	
	ff.action = 'interViewList.jsp';
	ff.submit();
 }
  
  function moveView(search, nowPage,serial,lk_code){
	  var ff = document.lk_interFrm;
	  ff.search.value=search;
	  ff.nowPage.value= nowPage;	
	  ff.serial.value = serial;
	  ff.lk_code.value = lk_code;
	  
	  ff.action="InterView.jsp";	
      ff.submit();
  }
</script>
</head>
<body>

<jsp:useBean id="dao" class="bean.Lk_InterViewDao" scope="page"/>
<jsp:setProperty property="*" name="dao"/>

<jsp:useBean id="vo" class="bean.Lk_InterViewVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<%

 String search = "";
String code = request.getParameter("lk_code");
String url = "../academy/images/";

 List<Lk_InterViewVo> list = dao.list(vo.getLk_code());
 
 
 
// 검색 버튼이 클릭된 경우 :검색 후 수정이나 삭제처리 후 리스트 화면으로 이동할 때 검색어 유지하기
if(request.getMethod().equals("POST")){
   search = dao.getSearch();   
} 
%>

<%=code %>

<h1>인터뷰List</h1>


<div id="section">
    <div id='search'>
		<input type='button' value='view' id='btnView' /> <!-- // ==> 새창 안에서의 view창으로 이동이 아니라 전의 view창으로 갈 수 있는 방법은 없을까? -->
		
		<form name='frm' method='post' action='interViewList.jsp' class="cdfrm">			
			<input type='text' name='search' id='search' value= "<%=search%>"/>
			<input type='submit' value='검색'/>
		</form>
	</div>
	

	<%
     for(int i=0; i<list.size() ; i++){ 	
		Lk_InterViewVo v = list.get(i);
 
%>
	<div id="interviewList">

	  <div class="list1">
	    <span class="listimg">
	       <a href="#" onclick = "moveView('<%=search%>','<%=dao.getNowPage()%>','<%=v.getSerial()%>',<%=v.getLk_code()%>)">
	         <img src="<%=url %><%=v.getFile1() %>"  data-holder-rendered="true"  style="width: 600px; height: 250px;"/>
	       </a>
	    </span>
	    <span class= "subcontent">
	     <div class="listsub">
	        <a href="#" onclick="moveView('<%=search%>','<%=dao.getNowPage()%>','<%=v.getSerial()%>')">
	           <%=v.getLk_subject() %>
	        </a>
	     </div>
	     <div class="listcon">
	        <%=v.getLk_content() %>
	     </div>
	    </span>
	  </div>
	 	
	</div>	
<%}%>	
		
	<div id='page'>
	   <div id='page_div'>	
		<%
		// 맨처음, 이전 버튼
		if(dao.getNowBlock()>1){%>
			<input type='button' value='맨처음' 
					 onclick="moveList('<%=search%>', '<%=1%>')"/>
			<input type='button' value='이전' 
					 onclick="moveList('<%=search%>', '<%=dao.getStartPage()-1%>')"/>
		<%}
		
		  for(int i=dao.getStartPage() ; i<=dao.getEndPage() ; i++){ %>
			<input type='button' value='<%=i %>' 
					 onclick="moveList('<%=search%>', '<%=i%>')"/>
					
		<%}
		  
		// 맨끝, 다음 버튼
		if(dao.getNowBlock() < dao.getTotBlock()){%>
			<input type='button' value='다음' 
					 onclick="moveList('<%=search%>', '<%=dao.getEndPage()+1%>')"/>
			<input type='button' value='맨끝' 
					 onclick="moveList('<%=search%>', '<%=dao.getTotPage()%>')"/>
		<%}%>

	</div>
  </div>
  	
</div>

</body>
<%@include file="lkInter_storeForm.jsp" %>
<script>interview('<%=vo.getLk_code()%>');</script>
</html>








