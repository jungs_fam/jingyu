<%@page import="bean.AcademyVo"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>자격증 소분류 리스트</title>
</head>
<body>
<%
int l_li = Integer.parseInt(request.getParameter("lee_l_li")); //학문 대분류 값 받음
//자격증 소분류
String sql = "";
PreparedStatement ps = null;
ResultSet rs = null;
Connection conn;

StringBuilder sb = new StringBuilder();

		try{ 
			conn = new DBConn().getConn();

			sql="select * from lk_license "
			  + "where l_license=? and s_license is not null "
			  + "order by license_name asc";
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, l_li);
			  
			  rs = ps.executeQuery();
				int o = 1;
			while(rs.next()){
				int i = rs.getInt("s_license");
				if(i == 0){}else{
				sb.append("<label><input type='radio' name='lee_s_li"+i+"' value='"+rs.getInt("s_license") +"'>" + rs.getString("license_name") + "</label>&nbsp;&nbsp;&nbsp;");
				}
				if(o%10 == 0){sb.append("<br/>");}
				
				o++;
			}
			
			out.print(sb.toString());
			
		}catch (Exception ex){
			ex.printStackTrace();
		}
%>
</body>
</html>