<%@page import="bean.AcademyDao"%>
<%@page import="java.util.List"%>
<%@page import="bean.AcademyVo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%String mid = (String)session.getAttribute("ip_mid"); %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- <link rel='stylesheet' href='academy/temp.css'> -->
<link href="academy/css/list.css" rel="stylesheet">
<style type="text/css">



</style>
<script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
var xhr; // 모든 곳에서 써야하기 때문에 var를 붙여서 준적역으로 생성
var xhr2;
var rs;

// AJAX 관련
function ms_story(serial){	
	if(window.ActiveXObject){		
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
		
	}
	
	    if(xhr == null) return;
		
	    var mid = document.th_storeForm.th_mid.value;
	    var url = 'academy/ajax/input_story.jsp';		
		url += '?mid='+ mid;
		url += '&serial=' + serial;
		xhr.open('get', url, true);
		xhr.send();
		
		alert("즐겨찾기에 등록되었습니다!")
			
	
}

function ms_loc(cval){	
	if(window.ActiveXObject){		
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
		
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/local_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc_loc;
		xhr.open('get', url, true);
		xhr.send();		
			
	
}


function ms_loc2(cval){	
	
	
	if(window.ActiveXObject){		
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;	  
	    var cval2 = document.getElementById("l_loc").value;
	    var url = 'academy/ajax/search.jsp';		
		url += '?category_val1=' + cval;
		url += '&category_val2=' + cval2;
		xhr.onreadystatechange = myFunc_loc2;
		xhr.open('get', url, true);
		xhr.send();		
			
	
}


function ms_search(){	
	
	
	if(window.ActiveXObject){		
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
		xhr2 = new ActiveXObject("Msxml2.XMLHTTP");
	} else {
		xhr = new XMLHttpRequest();
		xhr2 = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;	 
	    var z = document.getElementById("result_loc");
	    var y = document.getElementById("l_loc");	    
	    var t = document.getElementById("result_hak");
	    var x = document.getElementById("result_lic");
	    var cval = '-1';
	    var cval2 = '-1';
	    
	    if(z.value != '') {
	        cval = z.value;
	        cval2 = y.value;
	    }	//document.getElementById("result_loc").value;
	    var cval3 = '';
	    var cval4 = '';
	    if(t.value != ''){
	    	 cval3 = t.options[t.selectedIndex].text;
	    }
	    if(x.value != ''){	    	
	         cval4 = x.options[x.selectedIndex].text;
	    }
	   // var cval2 = document.getElementById("l_loc").value;
		
	    var url = 'academy/ajax/search.jsp';		
		url += '?category_val1=' + cval;
		url += '&category_val2=' + cval2;	    
        
		url += '&hak_s=' + cval3;        	
       
                	
		url += '&lic_s=' + cval4;
        
		xhr.onreadystatechange = myFunc_loc2;
		xhr.open('get', url, true);
		xhr.send();
}

// 자격증 카테고리
function ms_lic(cval){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/license_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc_lic;
		xhr.open('get', url, true);
		xhr.send();
}

// 학문 카테고리
function ms_hak(cval){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/hak_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc_hak;
		xhr.open('get', url, true);
		xhr.send();
}

//수신된 결과를 result에 표시
function myFunc_lic(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result_lic');
		rs.innerHTML = xhr.responseText;
	}
}

function myFunc_hak(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result_hak');
		rs.innerHTML = xhr.responseText;
	}
}
//수신된 결과를 result에 표시
function myFunc_loc(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result_loc');
		rs.innerHTML = xhr.responseText;
	}
}

function myFunc_loc2(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('ac_list');
		rs.innerHTML = xhr.responseText;
	}
}

function myFunc_page(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('page_com');
		rs.innerHTML = xhr.responseText;
	}
}

</script>

<script>
function moveInput(val){
	var url = "academy/last_input.jsp?im_id=" + val;
	var win = window.open(url, "window팝업", "width=1000, height=900"
			+ ", menubar=no, status=no, toolbar=no");
	
	
}

function moveView(val, val2){
	var url = "academy/view.jsp?lk_code=" + val + '&imid=' + val2;
	var win = window.open(url, "window팝업", "width=1000, height=900"
			+ ", menubar=no, status=no, toolbar=no");
	
	
}

// StoreForm 관련
function set_storeForm(mid) {
	var ff = document.th_storeForm;
	ff.th_mid.value = mid;
}
</script>

<script>
//콤보박스 이벤트
function sel_loc() {
	var ff = document.frm;
	var val_s = document.getElementById("result_loc").value;
	var val_l = document.getElementById("l_loc").value;
	ff.l_loc.value = val_l;
	ff.s_loc.value = val_s;
    ff.submit();		
    

}

//function sel_lic() {
//	var ff = document.frm_search;
//	var val = document.getElementById("result_lic").value;
//	ff.search.value = val;
 //  ff.submit();	
   

//}

//function sel_hak() {
//	var ff = document.frm_search;
//	var val = document.getElementById("result_hak").value;
//	ff.search.value = val;
//    ff.submit();		

// 콤보박스 값 유지 관련


function loc(val) {
	var gg = document.frm.l_loc;
	gg.selected.value = val;
}

function gradeSetting(grade) {
	var g = document.frm.l_loc;
	g.selectedIndex = Number(grade)-1;
}
//window.onload=function(){
//Object = document.getElementsByTagName('select')[1];
//↑ページ内のselectの[0]番目を得る
//Object.onchange=gradeSetting}
//↑onchangeイベントの処理を追加する
//}
</script>   
</head>
<body>
<jsp:useBean id="dao" class="bean.AcademyDao" scope="page"/>
<jsp:setProperty property="*" name="dao"/>
<jsp:useBean id="vo" class="bean.AcademyVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>
<%
String url = "academy/images/";


List<AcademyVo> local_list = dao.local_list();
List<AcademyVo> license_list = dao.license_list();
List<AcademyVo> hak_list = dao.hak_list();
List<AcademyVo> list = dao.list();
AcademyDao da = new AcademyDao();



//if(request.getMethod().equals("POST")){
//	search = dao.getSearch();

//}

%>

<div id='t_contents'>

<!-- 콤보박스 HTML -->
<div id='selection'>
<div id='select_m'>
<form name='frm' method='post' action='index.jsp?sub=academy/list.jsp'>
  <table id='table_sel'>
	<tbody>
		<tr>
			<td><font style='margin-right: 20px; font-weight: bold; font-size: 120%;'>지역</font></td>
			<td>
			  <div class="tab-tit">
                <select id='l_loc' name='l_loc' onchange='ms_loc(this.value)' >
                  <option value>지역 선택</option>
<%                
	              for(int i=0; i<local_list.size(); i++) {
		             AcademyVo v = local_list.get(i);

%>                   
                  <option value='<%=v.getL_doid() %>' >
                  <%=v.getLocal_name() %>
                  </option>        
<%} %>        
                </select>
              </div>
              
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result_loc' name='s_loc' onchange='ms_search();'>
			      <option  value>대분류를 먼저 선택해주세요</option>			    
			    </select>
			  </div>
			</td>
		</tr>
		<tr>
			<td><font style='margin-right: 20px; font-weight: bold; font-size: 120%;'>학문</font></td>
			<td>
			  <div class="tab-tit">
                <select id='l_hak' onChange='ms_hak(this.value);'>
                  <option value>학문 선택</option>
<%        
	              for(int i=0; i<hak_list.size(); i++) {
		             AcademyVo v = hak_list.get(i);
%>      
                  <option value="<%=v.getL_code() %>">
                  <%=v.getLearning_name() %>
                  </option>        
<%} %>        
                </select>
              </div>
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result_hak' onchange='ms_search();'>		    
			    </select>
			  </div>
			</td>
		</tr>
		<tr>
			<td><font style='margin-right: 20px; font-weight: bold; font-size: 120%;'>자격증</font></td>
			<td>
			  <div class="tab-tit">
                <select id='l_lic' onChange='ms_lic(this.options[this.selectedIndex].value);'>
                  <option value>자격증 선택</option>
<%        
	              for(int i=0; i<license_list.size(); i++) {
		             AcademyVo v = license_list.get(i);
%>      
                  <option value="<%=v.getL_license() %>">
                  <%=v.getLicense_name() %>
                  </option>        
<%} %>        
                </select>
              </div>
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result_lic' onchange='ms_search();'>		    
			    </select>
			  </div>
			</td>
		</tr>
	</tbody>
  </table>

</form>
</div>
<%
// 관리자 계정이 아닐시 삭제, 수정 버튼 숨기기
String smid = "im";
String secur = "";
String dsp = "";
String imid = (String)session.getAttribute("im_id");


if(mid == null && imid == null) { 
	dsp = "none";
} else { dsp=""; }


if(imid == null){
	imid=""; 
}

if(!imid.equals(smid)) { 
	secur = "none";
	} else { secur=""; }
%>

<div id='input_b' style='display:<%=secur %>'>
  관리자 전용 <br/></p>
  <input type='button' class='btns' onclick="moveInput('<%=imid %>')" value='등록'>
</div>  
</div><hr/>

<!-- list 영역 -->

    <!-- 학원 List 영역 -->
    <div id='ac_list'>
    <ul id='list_ul' class='paging'>    
    <%
	   for(int i=0; i<list.size(); i++) {
		   AcademyVo v = list.get(i); %>
  <li>
  <div class="ac_list">
  
     <div class="t_mainl">
        <div class="box1l">
           <a href="#" onclick="moveView('<%=v.getLk_code() %>', '<%=imid %>')">
           <img src="<%=url%><%=v.getLk_logo() %>" data-holder-rendered="true"  
                     style="width: 210px; height: 150px;"/></a>
         </div>
      </div>        
      <div class="t_mainc">
            <div class="box1r">
              <div class="sbox1">
                <a href="#" onclick="moveView('<%=v.getLk_code()%>','<%=imid %>' )"><%=v.getLk_name() %></a>
                &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;지역: <%=v.getLk_address() %>
              </div>
              <div class="sbox2">
                <input type='button' class='btns' value='favorite' 
                  onclick='ms_story(<%=v.getLk_code() %>)' style='display: <%=dsp %>'/>
              </div>
            </div>
            <div class="box2r"><%=v.getLk_cul() %></div>
            <div class="box3r">
            <%=v.getLk_txt() %>
            </div>
      </div>


  </div>
  </li>
  
<%} %> 
    </ul>
    </div>
 </div>
<script>
(function($) {

$.fn.quickPager = function(options) {

    var defaults = {
        pageSize: 4,
        currentPage: 1,
        holder: null,
        pagerLocation: "after"
    };

    var options = $.extend(defaults, options);


    return this.each(function() {


        var selector = $(this); 
        var pageCounter = 1;
        var x = 0;

        if( selector.parent("div.simplePagerContainer").length == 0 ) {
            selector.wrap("<div class='simplePagerContainer'></div>");
        }

        selector.parents(".simplePagerContainer").find("ul.simplePagerNav").remove();

        selector.children().each(function(i){ 
            $(this)[0].className = $(this)[0].className.replace(/\bsimplePagerPage.*?\b/g, '');
            if(!$(this).is(":hidden")) {
//                 if(x < pageCounter*options.pageSize && x >= (pageCounter-1)*options.pageSize) {
                    $(this).addClass("simplePagerPage"+pageCounter);
                }
                else {
                    $(this).addClass("simplePagerPage"+(pageCounter+1));
                    pageCounter ++;
                }   
                x++;
//             }
        });

        // show/hide the appropriate regions 
        selector.children().hide();
        selector.children(".simplePagerPage"+options.currentPage).show();

        if(pageCounter <= 1) {
            return;
        }

        //Build pager navigation
        var pageNav = "<ul class='simplePagerNav'>";

        for (i=1;i<=pageCounter;i++){
            if (i==0) {

            }
            if (i==options.currentPage) {
                pageNav += "<li class='currentPage simplePageNav"+i+"'><a rel='"+i+"' href='#'>"+i+"</a></li>"; 
            }
            else {
                pageNav += "<li class='simplePageNav"+i+"'><a rel='"+i+"' href='#'>"+i+"</a></li>";
            }
        }
        
        pageNav += "</ul>";

        if(!options.holder) {
            switch(options.pagerLocation)
            {
            case "before":
                selector.before(pageNav);
            break;
            case "both":
                selector.before(pageNav);
                selector.after(pageNav);
            break;
            default:
                selector.after(pageNav);
            }
        }
        else {
            $(options.holder).append(pageNav);
        }

        //pager navigation behaviour
        selector.parent().find(".simplePagerNav a").click(function() {

            //grab the REL attribute 
            var clickedLink = $(this).attr("rel");
            options.currentPage = clickedLink;

            if(options.holder) {
                $(this).parent("li").parent("ul").parent(options.holder).find("li.currentPage").removeClass("currentPage");
                $(this).parent("li").parent("ul").parent(options.holder).find("a[rel='"+clickedLink+"']").parent("li").addClass("currentPage");
            }
            else {
                //remove current current (!) page
                $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("li.currentPage").removeClass("currentPage");
                //Add current page highlighting
                $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("a[rel='"+clickedLink+"']").parent("li").addClass("currentPage");
            }

            //hide and show relevant links
            selector.children().hide();         
            selector.find(".simplePagerPage"+clickedLink).show();

            return false;
        });
    });
}
})(jQuery);

$("ul.paging").quickPager();

var nextLink = '<li><a id="nextLink" href="#">Next</a></li>';
var prevLink = '<li><a id="prevLink" href="#">Prev</a></li>';
$(".simplePagerNav").prepend(prevLink).append(nextLink);
$("#nextLink").click(function(e) {
    e.preventDefault();
    $("li.currentPage").next("li[class^=simplePageNav]").find("a").click();
});
$("#prevLink").click(function(e) {
    e.preventDefault();
    $("li.currentPage").prev("li[class^=simplePageNav]").find("a").click();
});
</script>
<%@include file="th_storeForm.jsp" %>
<script>set_storeForm('<%=mid %>');</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- <script>
    var tabTit = $(".tab-tit > ul > li"); // 버튼 영역을 변수 tabTit 할당
    var tabCont = $(".tab-cont > div");  // content 영역을 변수 tabCont 할당
    
    tabTit.click(function(){    // 클릭 이벤트 생성
      var target = $(this);    // 클릭한 버튼의 타겟을 변수 target에 할당
      var tc = target.find(">a");  // 버튼의 자식 요소 a 태그를 tc에 할당
      tabTit.find(">a").removeClass("active");
      tc.addClass("active");

      
    });
    
</script> -->
</body>

</html>