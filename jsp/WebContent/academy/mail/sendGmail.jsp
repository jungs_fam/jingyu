<%@page import="javax.mail.Transport"%>
<%@page import="javax.activation.DataHandler"%>
<%@page import="javax.activation.FileDataSource"%>
<%@page import="javax.mail.internet.MimeMultipart"%>
<%@page import="javax.mail.Multipart"%>
<%@page import="javax.mail.internet.MimeBodyPart"%>
<%@page import="java.util.Date"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@page import="javax.mail.Message"%>
<%@page import="bean.MyGoogle"%>
<%@page import="javax.mail.Session"%>
<%@page import="javax.mail.PasswordAuthentication"%>
<%@page import="com.sun.mail.util.MailSSLSocketFactory"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.sun.jmx.snmp.Enumerated"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h3>Google smtp로 전송</h3>
	<%
		String tempDir = "C:/workspace/final/WebContent/upload";
		int size = 1024 * 10000;

		/*첨부파일 임시로 upload*/
		MultipartRequest multi = new MultipartRequest(request, tempDir, size, "utf-8",
				new DefaultFileRenamePolicy());

		String sender = multi.getParameter("sender");
		String receiver = multi.getParameter("receiver");
		String subject = multi.getParameter("subject");
		String content = multi.getParameter("content");

		Enumeration e = multi.getFileNames();
		String file = (String) e.nextElement();
		String attfile = multi.getFilesystemName(file);

		/* 메일 전송 */
		String host = "smtp.gmail.com";

		try {
			/* 메일서버 설정 캡슐화 */
			Properties prop = new Properties();
			MailSSLSocketFactory msf = new MailSSLSocketFactory();
			msf.setTrustAllHosts(true);

			prop.put("mail.smtp.ssl.socketFactory", msf);
			prop.put("mail.smtp.starttls.enable", "true");
			prop.put("mail.smtp.ssl.enable", "true");
			prop.put("mail.transport.protocol", "smtp");
			prop.put("mail.smtp.port", "465");
			prop.put("mail.smtp.auth", "true");

			prop.put("mail.smtp.user", receiver);
			prop.put("mail.smtp.host", host);

			/* 사용하려는 메일 서버 권한자 지정 */
			Session mailSession = Session.getInstance(prop, new MyGoogle());
			
			/* 보내는 메시지 캡슐화 */
			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(sender));
			message.setSubject(subject);
			message.setSentDate(new Date());
			message.setContent(content, "text/html;charset=utf-8");
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));

			/* 첩부 파일 처리 */
			if (attfile != null) {
				MimeBodyPart body = new MimeBodyPart();
				Multipart part = new MimeMultipart();

				FileDataSource fds = new FileDataSource(tempDir + attfile);
				body.setDataHandler(new DataHandler(fds));
				body.setFileName(fds.getName());

				part.addBodyPart(body);
				message.setContent(part);
			}

			Transport.send(message);

		} catch (Exception ex) {
			out.print(ex.toString());
		}

		/*임시 파일 삭제 */
		try {
			File f = new File(tempDir + "/" + attfile);
			if (f.exists())
				f.delete();
		} catch (Exception ex) {
			out.print(ex.toString());
		}
	%>

	<ul>
		<li><%=sender%></li>
		<li><%=receiver%></li>
		<li><%=subject%></li>
		<li><%=content%></li>
		<li><%=attfile%></li>
	</ul>
</body>
</html>