<%@page import="bean.AcademyVo"%>
<%@page import="bean.AcademyDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>메일 보내기</title>
<script>
function init(){
	var url ="sendGmail.jsp";
	var ff = document.mailForm;
	ff.onsubmit = function(){
	if(ff.smtp[0].checked) url = "sendNaver.jsp";
	ff.action = url;
	ff.submit();
	}
	
}

</script>
<style>
 #mail{
  border:1px solid gray;
 }
</style>
</head>
<body>

<jsp:useBean id="vo" class="bean.AcademyVo" scope="page"/>
<jsp:setProperty property="*" name="vo"/>

<%
String code = request.getParameter("lk_code");

  AcademyDao dao = new AcademyDao();
  AcademyVo v = dao.view(vo.getLk_code());
%>

<%=code %>
<div id="mail">
<h3>담당자 문의</h3>
<form name='mailForm' method='post' enctype="multipart/form-data">

<lable>학원 관리자</lable>
<input type='text' name='Areceiver' value='<%=v.getLk_email()%>'/><br/>

<lable> 서버 관리자</lable>
<input type='text' name='Sreceiver' value='skscjswo0324@naver.com'/><br/>

<lable>발신자</lable>
<input type='text' name='sender'/><br/>

<label>제목</label>
<input type='text' name='subject'/><br/>

<label>내용</label><br/>
<textarea name='content' rows="10" cols="70"></textarea><br/>

<label>첨부파일</label>
<input type='file' name='attfile'/><p/>

<span>전송서버 선택</span>
[<label><input type='radio' name='smtp' value='naver' checked >네이버</label>
<label><input type='radio' name='smtp' value='gmail'>Gmail</label>]

<p/>

<input type='submit' value='메일전송'/>


</form>
</div>
<script>init();</script>
</body>
</html>