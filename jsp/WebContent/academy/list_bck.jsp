<%@page import="java.util.List"%>
<%@page import="bean.AcademyVo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='academy/temp.css'>
<style type="text/css">

    li {list-style:none;}
    a {text-decoration:none;}
    .tab-menu {
               width:700px; height: 120px; position: absolute; 
               display: block; left: 0px; top: -20px; 
    }
    .tab-cont {
                width:600px; height:50px; overflow:hidden;
                left:250px; top: 200px; 
                position: absolute; margin:0; padding:0; z-index: 3;
    }
    .tab-tit { width: 250px; }
    .tab-tit select{ width: 150px; }
    .s_catego { width: 250px; }
    .s_catego select{ width: 150px }
    #s_cat{display: list-item; z-index: 999;}

#t_contents{
    width: 960px;
    display: block;
    position: absolute;
    top: 300px;
}

.t_search
	{
	width:100%; background-color: #f2d5cf;
	height: 200px; z-index: 1;
	position: absolute; top: 150px;
	}
	
#input_b{
    height: 10px; text-align:right;
    display: block; position: absolute;
    right: 80px; top: 182px;
    margin: 0; padding: 0; z-index: 10;
}

.ac_list{

    position: relative;
    top: 150px;
}
.lctg{
  display: inline-block;

}

.t_mainl
	{
	float:left;
	width:30%;
	background-color: #D1F0FF;
	height: 150px;
	}

.r_mainr
	{
	float:left;
	width:70%;
	background-color: #FFE2B2;
	height: 150px;
	}

.box1l
	{
	width:100%;
	background-color: #D1F0FF;
	height: 100px;
	}

.box2l
	{
	width:100%;
	background-color: #FFE2B2;
	height: 50px;
	}
	
	
.box1r
	{
	width:100%;
	background-color: #FFE2B2;
	height: 50px;
	}

.box2r
	{
	width:100%;
	background-color: #D1F0FF;
	height: 50px;
	}
.box3r
	{
	width:100%;
	background-color: #FFE2B2;
	height: 50px;
	}
.off{
  display: none;
}
</style>

<script>
var xhr; // 모든 곳에서 써야하기 때문에 var를 붙여서 준적역으로 생성
var rs;

// 지역 카테고리
function ms_loc(cval){			
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/local_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc;
		xhr.open('get', url, true);
		xhr.send();
	
}

// 자격증 카테고리
function ms_lic(cval){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/license_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc_lic;
		xhr.open('get', url, true);
		xhr.send();
}

// 학문 카테고리
function ms_hak(cval){
	if(window.ActiveXObject){
		xhr = new ActiveXObject("Msxml2.XMLHTTP");  // 이 if, else 문장은 기본적으로 동일
	} else {
		xhr = new XMLHttpRequest();
	}
	
	    if(xhr == null) return;
		
	    var url = 'academy/ajax/hak_result.jsp';		
		url += '?category_val=' + cval;
		xhr.onreadystatechange = myFunc_hak;
		xhr.open('get', url, true);
		xhr.send();
}

//수신된 결과를 result에 표시
function myFunc_lic(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result_lic');
		rs.innerHTML = xhr.responseText;
	}
}

function myFunc_hak(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result_hak');
		rs.innerHTML = xhr.responseText;
	}
}
//수신된 결과를 result에 표시
function myFunc(){
	if(xhr.readyState == 4 && xhr.status == 200){
		rs = document.getElementById('result');
		rs.innerHTML = xhr.responseText;
	}
}
</script>

<script>

function moveInput(mid){
	var url = "academy/last_input.jsp"
	var win = window.open(url, "window팝업", "width=1000, height=900"
			+ ", menubar=no, status=no, toolbar=no");
	
// 	opener.document.lee_frm.ip_code.value = $("#mid").val();
//     self.close();
	
// 	alert(mid);
}

function moveView(){
	var url = "academy/view.jsp"
	var win = window.open(url, "window팝업", "width=1000, height=900"
			+ ", menubar=no, status=no, toolbar=no");
	
}

function sCategory(chk){
	var cell = document.getElementById('sctg');
	if ( chk.checked == true ){
	    cell.className = 'on';
	} else {
		cell.className = 'off';
	 }
}
</script>
</head>
<body>
<jsp:useBean id="dao" class="bean.AcademyDao" scope="page"/>
<jsp:setProperty property="*" name="dao"/>
<%

String url = "academy/images/";

List<AcademyVo> local_list = dao.local_list();
List<AcademyVo> license_list = dao.license_list();
List<AcademyVo> hak_list = dao.hak_list();
List<AcademyVo> list = dao.list();

String search = "";

if(request.getMethod().equals("POST")){
}


%>
String mid =(String)session.getAttribute("ip_mid");
<div id='input_b'>
  <input type='button' onclick='moveInput()' value='등록' />
</div>
<div id='t_contents'>
  <table>
	<tbody>
		<tr>
			<td>
			  <div class="tab-tit">
                <select onChange='ms_loc(this.value);'>
<%                
	              for(int i=0; i<local_list.size(); i++) {
		             AcademyVo v = local_list.get(i);
%>      
<%} %>        
                </select>
              </div>
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result'  onChange='sel_loc(this.options[this.selectedIndex].value)'>
			    </select>
			  </div>
			</td>
		</tr>
		<tr>
			<td>학문</td>
			<td>
			  <div class="tab-tit">
                <select onChange='ms_hak(this.value);'>
<%        
	              for(int i=0; i<hak_list.size(); i++) {
		             AcademyVo v = hak_list.get(i);
%>      
<%} %>        
                </select>
              </div>
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result_hak'>
			    </select>
			  </div>
			</td>
		</tr>
		<tr>
			<td>자격증</td>
			<td>
			  <div class="tab-tit">
                <select onChange='ms_lic(this.options[this.selectedIndex].value);'>
<%        
	              for(int i=0; i<license_list.size(); i++) {
		             AcademyVo v = license_list.get(i);
%>      
<%} %>        
                </select>
              </div>
			</td>
			<td>
			  <div class='s_catego'>
			    <select id='result_lic'>
			    </select>
			  </div>
			</td>
		</tr>
	</tbody>
  </table>
        
  <div class="ac_list">
  
     <div class="t_mainl">
        <div class="box1l">
                     style="width: 200px; height: 130px;"/>
         </div>
         <div class="box2l">
           
         </div>
      </div>        
      <div class="t_mainr">
            <div class="box2r">ボックス２</div>
            <div class="box3r">ボックス3</div>
      </div>

  </div>
  
  
  </div>

<div id='search'>
   <form name='frm' method='post' action='Index.jsp?sub=academy/list.jsp'>
      <label>검색</label>
      <input type='submit' class='btn' value='검색'/>   
   </form>
</div>

<%@include file="storeForm.jsp" %>
<script>
//콤보박스 이벤트
function sel_loc(val) {
	var ff = document.storeForm;
	ff.search.value = val;

    	ff.action = "index.jsp?sub=list.jsp";
    	ff.submit();
		

}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
<!-- <script>
    var tabTit = $(".tab-tit > ul > li"); // 버튼 영역을 변수 tabTit 할당
    var tabCont = $(".tab-cont > div");  // content 영역을 변수 tabCont 할당
    
    tabTit.click(function(){    // 클릭 이벤트 생성
      var target = $(this);    // 클릭한 버튼의 타겟을 변수 target에 할당
      var tc = target.find(">a");  // 버튼의 자식 요소 a 태그를 tc에 할당
      tabTit.find(">a").removeClass("active");
      tc.addClass("active");

      
    });
    
</script> -->
</body>
</html>