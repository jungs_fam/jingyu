<%@page import="java.sql.ResultSet"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div id='s_cat'>
  <ul>

<%
int category_val1 = Integer.parseInt(request.getParameter("category_val1"));
int category_val2 = Integer.parseInt(request.getParameter("category_val2"));

//페이지를 분리하기 위한 변수들
int listSize = 10; // 한페이지에 표시될 건수
int blockSize = 4;

int totSize = 0; // 검색된 결과의 전체 건수
int totPage = 0; // topSize, listSize의 절상 결과(Math.ceil())

int totBlock = 0;  // totPage, blockSize의 절상
int nowBlock = 0;  // 현재 보고있는 블럭 번호

int nowPage = 1;   // 현재 보고있는 페이지
int startNo = 0;  // endNo - listSize + 1 startNo = endNo - listSize + 1
int endNo = 0;  // nowPage * listSize   endNo = nowBlock * blockSize

int endPage = 0;  // nowBlock * blockSize   endPage = nowBlock * blockSize
int startPage = 0;  // endPage - blockSize + 1   startPage = endPage - blockSize + 1

Connection conn;
PreparedStatement ps;
StringBuilder sb = new StringBuilder();

try{
	conn = new DBConn().getConn();
	String sql = "select count(*) totSize from lk_academy"
		       + " where lk_scityid=? or lk_bdoid=?";
	ps = conn.prepareStatement(sql);
	ps.setInt(1, category_val1);
	ps.setInt(2, category_val2);
	
	ResultSet rs = ps.executeQuery();	
	rs.next();
	totSize = rs.getInt("totSize");
	
	// 페이지 분리를 위한 변수 계산
	totPage = (int)Math.ceil(totSize/(float)listSize);
	totBlock = (int)Math.ceil(totPage/(float)blockSize);
	nowBlock = (int)Math.ceil(nowPage/(float)blockSize);
	
	endNo = nowPage * listSize;
	startNo = endNo - listSize + 1;
	
	endPage = nowBlock * blockSize;
	startPage = endPage - blockSize + 1;
	
	int temp = 1;
	int nowPages = nowPage-1;
	
	// 전체수보다 계산된 수가 더 클경우 보정
	if(endPage > totPage) endPage = totPage;
	

	if(nowBlock>1) {
	   sb.append("<input type='button' value='맨처음' onclick=" + "movePage('" + category_val1 + "'," + temp + " )/>" 
	    + "<input type='button' value='이전' onclick=" + "movePage('" + category_val1 + "', '" + nowPages + "')/>");
  } %>

 <% for(int i=startPage; i<=endPage; i++) { 
	   sb.append("<input type='button' value='" + i + "'"
	          + "onclick=" + "movePage('" + category_val1 + "', '" + i + "')/>");
   } %>
<%
// 맨끝, 다음 버튼
if(nowBlock < totBlock) { 
	sb.append("<input type='button' value='다음' onclick="+"movePage('" + category_val1 + "','" + endPage+1 + " ')/>"
			+ "<input type='button' value='맨끝' onclick=" + "movePage('" + category_val1 + "', '" + totPage +"')/>");
 }
	
	
	out.print(sb.toString());
	
} catch(Exception ex){
	ex.printStackTrace();
}
%>
  </ul>
</div>
</body>
</html>