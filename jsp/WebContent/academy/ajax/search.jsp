<%@page import="java.sql.ResultSet"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
int local_s = -1;
int local_l = -1;
String hak_l= " ";
String hak_s= "";
String lic_l= " ";
String lic_s= "";


if(request.getParameter("category_val1") != null) {
local_s = Integer.parseInt(request.getParameter("category_val1"));
local_l = Integer.parseInt(request.getParameter("category_val2"));
}

if(request.getParameter("hak_s") != "") {
hak_s = request.getParameter("hak_s");
//hak_l = request.getParameter("hak_l");
}

if(request.getParameter("lic_s") != "") {
lic_s = request.getParameter("lic_s");
//lic_l = request.getParameter("lic_l");
}

String url = "academy/images/";
String sql = "";
Connection conn;
PreparedStatement ps = null;
ResultSet rs = null;
StringBuilder sb = new StringBuilder();

String search = "";


//페이지를 분리하기 위한 변수들
int listSize = 10; // 한페이지에 표시될 건수
int blockSize = 4;

int totSize = 0; // 검색된 결과의 전체 건수
int totPage = 0; // topSize, listSize의 절상 결과(Math.ceil())

int totBlock = 0;  // totPage, blockSize의 절상
int nowBlock = 0;  // 현재 보고있는 블럭 번호

int nowPage = 1;   // 현재 보고있는 페이지
int startNo = 0;  // endNo - listSize + 1 startNo = endNo - listSize + 1
int endNo = 0;  // nowPage * listSize   endNo = nowBlock * blockSize

int endPage = 0;  // nowBlock * blockSize   endPage = nowBlock * blockSize
int startPage = 0;  // endPage - blockSize + 1   startPage = endPage - blockSize + 1

try{
	conn = new DBConn().getConn();

	
	endNo = nowPage * listSize;
	startNo = endNo - listSize + 1;
	
	
	// 전체수보다 계산된 수가 더 클경우 보정
	if(endPage > totPage) endPage = totPage;
	
	
	sql = "select * from("
		    + " select rownum no, s.* from("
			+ " select * from temps"
			+ " where lk_name like ? ";
	if(local_s != -1) {
			sql  += " and local_s=" + local_s + " and local_l=" + local_l; }
	if(hak_s != "") {
		sql  += " and learnings like " + "'%" + hak_s + "%'"; } 
	if(lic_s != "") {
		sql	 +=	" and licenses like " + "'%" + lic_s + "%'"; }
	sql		+= " order by lk_code"
		    + " 	)s"
		    + " ) where no between ? and ?";
	

	ps = conn.prepareStatement(sql);
	
	ps.setString(1, "%" + search + "%");
	ps.setInt(2, startNo);
	ps.setInt(3, endNo);	
	
	rs = ps.executeQuery();
	
	while(rs.next()){
		//sb.append("<li><input type='checkbox' value='" + rs.getInt("s_cityid") +"'/>" + rs.getString("local_name") + "</li>&nbsp;&nbsp;");		
		sb.append( 
		     "<li><div class='list_contents'><div class='t_mainl'>"
		    	        +"<div class='box1l'>"
		    	        +   "<a href='#' onclick='moveView(" +rs.getInt("lk_code")+ ");return false;'>"
		    	        +   "<img src='"+ url + rs.getString("lk_logo") + "' data-holder-rendered='true'" 
		    	        +             "style='width: 200px; height: 130px;'/>"
		    	        + "</div>"
		    	      +"</div>"        
		    	      +"<div class='t_mainr'>"
		    	      +      "<div class='box1r'><a href='#' onclick='moveView()'><h3>"+ rs.getString("lk_name")+ "</h3></a></div>"
		    	      +      "<div class='box2r'>ボックス２</div>"
		    	      +      "<div class='box3r'>"
		    	      + "<input type='button' class='btns' onclick='ms_story(" + rs.getInt("lk_code") + ")' value='Favorite'/>"  
		    	      +      "</div>"
		    	      +"</div></div></li><hr/>" 
		    			);
	} 
	

	out.print(sb.toString());
	
} catch(Exception ex){
	ex.printStackTrace();
}
%>

</body>
</html>