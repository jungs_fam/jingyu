<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
String mid = request.getParameter("mid");
String serial = request.getParameter("serial");
String url = "index.jsp?sub=academy/view.jsp?lk_code=" + serial;

Connection conn;
PreparedStatement ps = null;
StringBuilder sb = new StringBuilder();
int r = 0;
try{
	conn = new DBConn().getConn();
	String sql = "insert into lk_story(ip_mid, lk_url, lk_date) values(?, ?, sysdate)";
	ps = conn.prepareStatement(sql);
	ps.setString(1, mid);
	ps.setString(2, serial);
	
	r = ps.executeUpdate();
	
	
} catch(Exception ex){
	ex.printStackTrace();
} 
%>
</body>
</html>