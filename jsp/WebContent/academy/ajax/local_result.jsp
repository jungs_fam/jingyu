<%@page import="java.sql.ResultSet"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
    #s_cat ul {overflow:hidden;}
    #s_cat li {float:left; width:25%; text-align:center;}
</style>
</head>
<body>
<div id='s_cat'>

<%
int category_val = Integer.parseInt(request.getParameter("category_val"));

Connection conn;
PreparedStatement ps;
StringBuilder sb = new StringBuilder();

try{
	conn = new DBConn().getConn();
	String sql = "select * from lk_local where l_doid=? and s_cityid is not null order by s_cityid";
	ps = conn.prepareStatement(sql);
	ps.setInt(1, category_val);
	
	ResultSet rs = ps.executeQuery();
	
	while(rs.next()){
		//sb.append("<li><input type='checkbox' value='" + rs.getInt("s_cityid") +"'/>" + rs.getString("local_name") + "</li>&nbsp;&nbsp;");		
		sb.append("<option value='" + rs.getInt("s_cityid") +"'>" + rs.getString("local_name") + "</option>");
	}
	
	out.print(sb.toString());
	
} catch(Exception ex){
	ex.printStackTrace();
}
%>

</div>
</body>
</html>