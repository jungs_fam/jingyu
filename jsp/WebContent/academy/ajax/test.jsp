<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>
function set_matching_words(selectObj, txtObj)
{
   var letter = txtObj.value;
 
   for(var i = 0; i < selectObj.length; i++)
   {
      if(selectObj.options[i].value.charAt(0) == letter)
      {
         selectObj.options[i].selected = true;
      }
      else
      {
         selectObj.options[i].selected = false;
      }
   }
}​
</script>
</head>
<body>
<form name="check_letter_frm" action="#">
 
<label>Enter a letter: </label><input type="text" name="check_letter" size="1">
 
<input type="button" name="matching_word" value="Select matching Word" onclick="set_matching_words(this.form.letters, this.form.check_letter);">
 
Matching Word: <select name="letters">
 
<option></option>
<option value="apple">apple</option>
<option value="ant">ant</option>
<option value="ape">ape</option>
<option value="age">age</option>
<option value="boy">boy</option>
<option value="banana">banana</option>
<option value="carrot">carrot</option>
<option value="cat">cat</option>
<option value="daisy">daisy</option>
<option value="desk">desk</option>
 
</select>
</form> 
</body>
</html>