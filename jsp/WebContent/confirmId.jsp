<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="bean.DBConn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
#chk{
  border: none;
  background-color: #5c5c5c;
  color: white;
}
#war{
  color: red;
}

</style>
</head>
<%
      String mid = request.getParameter("ip_mid");
        
        int x = -1;

        String sql  = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			Connection conn = new DBConn().getConn();
			sql = "select * from i_person where ip_mid = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, mid);
			rs=ps.executeQuery();
			if(rs.next()){
			x=1;
			}else{
				x = -1;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}

%>

<body>
<%
      if(x==1)
      {
%>

<table width = "300" border = "0" cellspacing = "0" cellpadding = "5">
<tr>
      <td id="war" height = "39">[<%= mid %>] 이미 사용중인 아이디입니다.</td>
</tr>
</table>
<form name = "checkForm" method = "post" action = "confirmId.jsp">
<table width = "300" border = "0" cellspacing = "0" cellpadding = "5">
<tr>
      <td align = "center">
            다른 아이디를 사용하세요.<p>
            <input type = "text" size = "10" maxlength = "12" name = "ip_mid">
            <input id="chk" type = "submit" value = "ID중복확인">
      </td>
</tr>
</table>
</form>
<%
      }
      else
      {
%>

<table width = "300" border = "0" cellspacing = "0" cellpadding = "5">
<tr>
      <td align = "center"><p>
            입력하신 <%= mid %>는<br/> 사용하실 수 있는 ID입니다.<br/><br/>
            <input id="chk" type = "button" value = "닫기" onclick = "setid()">
      </td>
</tr>
</table>

<%
      }

%>

<script language = "javascript">
 
      function setid()
      {
            opener.document.frm.ip_mid.value = "<%= mid %>";
            self.close();
      }

</script>
</body>
</html>
