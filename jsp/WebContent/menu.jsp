<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.menubar{
border:none;
border:0px;
margin:0px;
padding:0px;
font: 67.5% "Lucida Sans Unicode", "Bitstream Vera Sans", "Trebuchet Unicode MS", "Lucida Grande", Verdana, Helvetica, sans-serif;
font-size:18px;
border-bottom: solid 4px #da2226;
z-index:800;
}

.menubar ul{
background: #303030;
height:50px;
list-style:none;
margin:0;
padding:0;
}

.menubar li{
float:left;
padding:0px;
}

.menubar li a{
background: #303030;
color:#cccccc;
display:block;
font-weight:normal;
line-height:50px;
margin-left:90px;
padding:0px 25px;
text-align:center;
text-decoration:none;
}


.menubar li a:hover, .menubar ul li:hover a{
 background: rgb(71,71,71);
 color:#FFFFFF;
 text-decoration:none;
 background-color: #da2226;
 padding-top: 4px;
 -webkit-transition-duration: 0.3s;
 transition-duration: 0.3s;

}

.menubar li ul{
background: rgb(109,109,109);
display:none; /* 평상시에는 드랍메뉴가 안보이게 하기 */
height:auto; 
padding:0px;
margin:0px;
border:0px;
position:absolute;
width:200px;
z-index:700;
margin-left:90px;
/*top:1em;
/*left:0;*/
}

.menubar li:hover ul{
display:block; /* 마우스 커서 올리면 드랍메뉴 보이게 하기 */

}

.menubar li li {
background: rgb(71,71,71);
display:block;
float:none;
margin:0px;
padding:0px;
width:200px;
}

.menubar li:hover li a{
background:none;
}

.menubar li ul a{
display:block;
height:50px;
font-size:14px;
font-style:normal;
margin:0px;
padding:0px 10px 0px 15px;
text-align:left;

}

.menubar li ul a:hover, .menubar li ul li:hover a{

background: rgb(109,109,109);
border:0px;
color:#da2226;
text-decoration:none;

}

.menubar p{
clear:left;
}

</style>
</head>
<body>

<div class="menubar">
   <ul>
       <li><a class="hvr-sweep-to-top" href="index.jsp">Academy Korea</a></li>
      <li><a class="hvr-sweep-to-top" href="index.jsp?sub=intro/intro.jsp">소개글</a></li>
      <li><a class="hvr-sweep-to-top" href="#" id="current">학원</a>
         <ul>
           <li><a href="index.jsp?sub=academy/list.jsp"><img src='img/Reading.png' height='25px' width='25px'>　　학문</a></li>
           <li><a href="index.jsp?sub=academy/list.jsp"><img src='img/Business Contact.png' height='25px' width='25px'>　　자격증</a></li>
           <li><a href="index.jsp?sub=academy/list.jsp"><img src='img/In Transit.png' height='25px' width='25px'>　　지역</a></li>
         </ul>
      </li>
      <li><a class="hvr-sweep-to-top" href="#">게시판</a>
          <ul> 
           <li><a href="index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=board/ej_boardlist.jsp"><img src='img/Collaboration.png' height='25px' width='25px'>　　자유 게시판</a></li>
           <li><a href="index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=event/ej_eventList.jsp"><img src='img/Idea.png' height='25px' width='25px'>　　이벤트 게시판</a></li>
          </ul>
          </li>
      <li><a class="hvr-sweep-to-top" href="index.jsp?sub=calendar/calendar.jsp">시험일정</a></li>
   </ul>
</div>

</body>
</html>