<%@page import="e_bean.eboardVo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="board/board/ej_boardlist.css">
<script src="board/board/ej_boardlist.js"></script>

 <jsp:useBean id="dao" class="e_bean.eboardDao" scope="page"></jsp:useBean>
<jsp:setProperty property="*" name="dao"/>

</head>
<body>
<%
String ej_sc = "";
List<eboardVo> list =dao.list();
List<eboardVo> ralist = dao.ev_list();

if(request.getMethod().equals("POST")){
	ej_sc = dao.getEj_sc();
	}

%>
<div id="ej_sce">

	<div id="ej_sc">
		<form name="ej1_frm" method="post">
			<input type="text" name="ej_sc" value="<%=ej_sc%>"/>
			<input type="submit" value="검색"/>
		</form>
	</div>

<div id="ej_main">
	<div id="ej_boader">
		<div class="ej_title">
			<span>글  번호</span>
			<span>제    목</span>
			<span>글 쓴 이</span>
			<span>작 성 일</span>
			<span>조 회 수</span>
		</div>
		<%
		for(int i=0; i<ralist.size(); i++){
		eboardVo v = ralist.get(i);
		
		int ej_serial = v.getE_serial(); 
	out.print("<div class='ej_date'>");
	out.print("<span>"+ej_serial+"</span>");
	%>
	
		<span><a href='#' onclick="moveView2('<%=ej_sc%>','<%=dao.getNowPage() %>','<%=ej_serial%>')"><%=v.getE_subject()%></a></span>
	<%
		out.print("<span>"+v.getE_mid()+"</span>");
		out.print("<span>"+v.getE_date()+"</span>");
		out.print("<span>"+v.getE_cnt()+"</span>");
		out.print("</div>");
		}
		%>
		<%
		for(int i=0; i<list.size(); i++){
			eboardVo v = list.get(i);
			int ej_serial = v.getE_serial(); 
		out.print("<div class='ej_row'>");
		out.print("<span>"+ej_serial+"</span>");
		%>
		
			<span><a href='#' onclick="moveView('<%=ej_sc%>','<%=dao.getNowPage() %>','<%=ej_serial%>')"><%=v.getE_subject()%></a></span>
		<%
			out.print("<span>"+v.getE_mid()+"</span>");
			out.print("<span>"+v.getE_date()+"</span>");
			out.print("<span>"+v.getE_cnt()+"</span>");
			out.print("</div>");
		}
		 %>
	<%if(dao.getNowBlock() > 1){%>
	<input type = 'button' value='맨처음' onclick="movePage('<%=ej_sc%>', '<%=1 %>')"/>
	<input type = 'button' value='이전' onclick="movePage('<%=ej_sc%>', '<%=dao.getNowPage()-1 %>')"/>
	
<%}



for(int i = dao.getStartPage(); i <= dao.getEndPage(); i++){%>
	<input type = 'button' value='<%=i %>' onclick="movePage('<%=ej_sc%>', '<%=i %>')"/>
	
<%}

//맨끝 그리고 다음페이지
if(dao.getNowBlock() < dao.getTotBlock()){ %>
	<input type = 'button' value='다음' onclick="movePage('<%=ej_sc%>', '<%=dao.getEndPage()+1 %>')"/>	
	<input type = 'button' value='맨끝' onclick="movePage('<%=ej_sc%>', '<%=dao.getTotPage() %>')"/>
	
<%}
%>
	</div>
	
	<div id='ej_but'>
		<input type="button" onclick="moveInsert('<%=ej_sc%>','<%=dao.getNowPage() %>')" value="글쓰기">
	</div>
	
</div>
</div>

<%@include file="../ej_Form.jsp" %>

<script >
//관리자 접속 코드
var asd = "asd";
// var asd = "rbwlswjd";
// var asd = null;

function moveView( sc , nowPage, serial){
	var url = 'index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=board/ej_view.jsp';
	var ff = document.ej_Form;
	ff.ej_sc.value = sc;
	ff.nowPage.value = nowPage;
	ff.e_serial.value = serial;
	ff.action = url;
	ff.submit();
}
function moveView2( sc , nowPage, serial){
	var url = 'index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=board/ej_view2.jsp';
	var ff = document.ej_Form;
	ff.ej_sc.value = sc;
	ff.nowPage.value = nowPage;
	ff.e_serial.value = serial;
	ff.action = url;
	ff.submit();
}


function moveInsert( sc , nowPage){
	var ff = document.ej_Form;
	ff.ej_sc.value = sc;
	ff.nowPage.value = nowPage;
	
	if(asd== "asd"){
	var url = 'index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=board/ej_ev_insert.jsp';
	ff.action = url;
	ff.submit();
	}else if(asd!=null){
	var url = 'index.jsp?sub=board/ej_index.jsp?ej_inc=ej_menu.jsp&ej_sub=board/ej_insert.jsp';
	ff.action = url;
	ff.submit();
	}else{
		alert("로그인 하셔야 글쓰기 작성이 가능 합니다.");
	}
}

//페이지 넘김 버튼
function movePage(sc,  nowPage) {
	var ff  = document.ej_Form;
	ff.ej_sc.value = sc;
	ff.nowPage.value = nowPage;
	ff.submit();
}

</script>

</body>
</html>