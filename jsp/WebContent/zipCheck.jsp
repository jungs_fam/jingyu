<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
#h_modi{
  border: 0 solid;
  width: 100px;
  float: left;
  margin-top: 5px;
}
#h_m1,#h_m2,#h_m3,#h_m4{
  margin-left: 10px;
  height: 20px;
  margin-top: 5px;
}
#postcodify_search_button{
  margin-top:10px;
  height: 50px;
  width: 100px;
  border: none;
  margin-left: 180px;
  background-color: #5c5c5c;
  color: white;
  font-weight:bold;
}
#code{
  margin-top:10px;
  height: 50px;
  width: 100px;
  border: none;
  margin-left: 65px;
  position: absolute;
  background-color: #da2226;
  color: white;
  font-weight:bold;
}
</style>
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="//d1p7wdleee1q2z.cloudfront.net/post/search.min.js"></script>
<body>
<%

String code = request.getParameter("ip_code");


%>
<!-- 주소와 우편번호를 입력할 <input>들을 생성하고 적당한 name과 class를 부여한다 -->
<div id='h_modi'>우편번호</div><input id="h_m1" type="text" name="code_1" class="postcodify_postcode5" /><br/>
<div id='h_modi'>도로명 주소</div><input id="h_m2" type="text" name="code_2" class="postcodify_address" /><br/>
<div id='h_modi'>상세주소</div><input id="h_m3" type="text" name="code_3" class="postcodify_details"/><br/>
<div id='h_modi'>참고사항</div><input id="h_m4" type="text" name="code_4" class="postcodify_extra_info"/><br/>

<input id="code" type="submit" value="입력" onclick ="setid()">
<button id="postcodify_search_button">주소검색</button>

<!-- "검색" 단추를 누르면 팝업 레이어가 열리도록 설정한다 -->
<script> $(function() { $("#postcodify_search_button").postcodifyPopUp(); }); </script>
</body>
<script language = "javascript">
 
      function setid()
      {
            opener.document.frm.ip_code.value = $("#h_m1").val();
            opener.document.frm.ip_address.value = $("#h_m2").val()+$("#h_m3").val()+$("#h_m4").val();
            self.close();
      }

</script>
</html>