<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
#log{
  height: 120px;
  text-align: center;
  background-color: #f8f8f8;
  padding-top: 50px;
}
#la2{
  font-size : 22px;
  background-color: #da2226;
  color: white;
  margin-left: 500px;
  padding: 20px;
  border: none;
}
</style>
</head>
<body>
<jsp:useBean id="vo" class="bean.o_FinalVo" scope="page"></jsp:useBean>
<jsp:setProperty property="*" name="vo"/>


<%

Connection conn = new DBConn().getConn();
PreparedStatement ps = null;

try{

	int r = 0;

	String sql = "insert into i_person values(?,?,?,?,?,'0',?,?,'')";

	ps=conn.prepareStatement(sql);
	ps.setString(1, vo.getIp_mid());
	ps.setString(2, vo.getIp_pwd());
	ps.setString(3, vo.getIp_irum());
	ps.setString(4, vo.getIp_phone());
	ps.setString(5, vo.getIp_email()+'@'+vo.getIp_domain());
	ps.setString(6, vo.getIp_code());
	ps.setString(7, vo.getIp_address());

	
	r=ps.executeUpdate();
	
 }catch(Exception ex){
	out.print(ex.toString());
}

%>
<div id="log">
<h1><%=vo.getIp_irum() %>님 회원 가입을 축하드립니다.</h1>
<h5>당신의 꿈을 응원하는 아카데미 코리아에 오신걸 환영합니다.</h5>
</div>
<br/><br/><br/><br/><br/>

<input id="la2" type="button" onclick="location.href='index.jsp?sub=login/login.jsp'" value="로그인 화면으로" style="cursor:pointer">
</body>
</html>