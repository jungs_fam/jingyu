<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
<style>
#log{
  height: 120px;
  text-align: center;
  background-color: #f8f8f8;
  padding-top: 50px;
}
#img{
  margin-top: 40px;
  margin-left: 500px;

}
#h_modi{
  border: 0 solid ;
  width: 100px;
  float: left;
  margin-left: 30px;
}
#h_m{
  margin-left: 20px;
} 

#wrap{
  height: 100px;
  border-top: solid gray 5px;
}
#id{
  padding-left:640px;
  padding-top: 15px;
  FONT-SIZE: 9pt; COLOR: gray; LINE-HEIGHT: 160%; 
  FONT-FAMILY: 굴림, verdana, tahoma
}
#pd{
  padding-left:640px;
  padding-top: 15px;
  FONT-SIZE: 9pt; COLOR: gray; LINE-HEIGHT: 160%; 
  FONT-FAMILY: 굴림, verdana, tahoma
}
#lo{
  float:left;
  height: 100px;
  width: 600px;
  border-right: solid gray 1px; 
  padding-top: 60px;
}
#la1{
  font-size : 15px;
  background-color: #5c5c5c;
  color: white;
  margin-left: 150px;
  padding: 5px 24px 5px 24px;
  border: none;
}
#la2{
  font-size : 15px;
  background-color: #5c5c5c;
  color: white;
  margin-left: 137px;
  padding: 5px 17px 5px 17px;
  border: none;
}
#su1{
  background-color: #da2226;
  color: white;
  font-weight:bold;
  height: 64px;
  width: 100px;
  float: right;
  margin-right: 30px;
  border: none;
  outline: none;

}

#su2{
  background-color: #5c5c5c;
  color: white;
  font-weight:bold;
  height: 64px;
  width: 70px;
  float: right;
  margin-right: 30px;
  border: none;
  outline: none;
  
}
</style>

</head>
<body>
<div id="log">
<h1>회원 로그인</h1>
<h5>당신의 꿈을 응원하는 아카데미 코리아에 오신걸 환영합니다.</h5>
</div>
<br/>
<form action="index.jsp?sub=login/login_result.jsp" method="post">
<img id="img" alt="" src="./img/member_login.png"><br/><br/><br/>
<div id="wrap">
 <div id="lo">
 <input id="su2" type="button" value="관리자" style="cursor:pointer" onclick="location.href='index.jsp?sub=login/master_login.jsp'">
 <input id="su1" type="submit" value="로그인" style="cursor:pointer">
<div id='h_modi'>아이디: </div><input id="h_m" type="text" name="ip_mid"placeholder="ID"><br/><br/>
<div id='h_modi'>비밀번호: </div><input id="h_m" type="password" name="ip_pwd"placeholder="PASSWORD">
<br/><br/>
</div>

<div id="id">
ID<br/>
아이디가 기억나지 않으세요?
<input id="la1" type="button" onclick="location.href='index.jsp?sub=login/searchMid.jsp'" value="아이디 찾기" style="cursor:pointer">
</div><br/>
<hr/>
<div id="pd">
PASSWORD<br/>
비밀번호가 기억나지 않으세요?
<input id="la2" type="button" onclick="location.href='index.jsp?sub=login/searchPwd.jsp'" value="비밀번호 찾기" style="cursor:pointer">
</div>
<br/><br/><hr/>
</div>
</form>
</body>
</html>