<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="bean.DBConn"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
try{
	
	String im_id = request.getParameter("im_id");
	String im_pwd = request.getParameter("im_pwd");

	Connection conn = new DBConn().getConn();
	PreparedStatement ps = null;
	
	String sql = "select * from i_master where im_id = ? and im_pwd = ?";

	ps = conn.prepareStatement(sql);
	ps.setString(1, im_id);
	ps.setString(2, im_pwd);
	
	ResultSet rs = ps.executeQuery();

	
	boolean isLogin = false;

	while(rs.next()){
		isLogin = true;
	}
	
	if(isLogin){
		session.setAttribute("im_id", im_id);
		session.setAttribute("im_pwd", im_pwd);
		out.print("<script>");
		out.print("location.href='index.jsp'");
		out.print("</script>");
		
	}else{
		
		
		%><script>alert("로그인 실패"); history.go(-1);</script><%
	}
	
	}catch (Exception e){
	    out.print("DB 연동 실패");	
	}	
%>

</body>
</html>