<%@page import="java.sql.Connection"%>
<%@page import="bean.DBConn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>input_person</title>
<style>
#bo1{
   border: none;

/*   border-color: white; */
}
#bo1 tr td{
   
   border-left: none;
   border-right: none;
   padding-top: 20px;
   padding-bottom: 20px;
   
}
#bo1 input{
   height: 25px;
}
#col input{
  border: none;
  outline: none;
  padding-left: 15px;
  padding-right: 15px;
  padding-bottom: 10px;
  padding-top: 5px;
  height: 30px;

}
#col2, #col3{
  border: none;
  outline:none;
  background-color: #5c5c5c;
  color: white;
  font-weight:bold;
  
}
#go{
  background-color: #da2226;
  color: white;
  font-weight:bold;
}
#war{
  border: none;
  color: red;

}
#up{
  border-top: none;

}
</style>
<script language = "javascript">
 
      // 아이디 중복 여부를 판단
      function openConfirmid(frm)
      {
            // 아이디를 입력했는지 검사
            if(frm.ip_mid.value == "")
            {
                  alert("아이디를 입력하세요");
                  return;
            }
   
            // url과 사용자 입력 아이디를 조합합니다.
            url = "confirmId.jsp?ip_mid=" + frm.ip_mid.value;
  
            // 새로운 윈도우를 엽니다.
            open(url, "confirm", 
                  "toolbar = no, location = no, status = no," + 
                  "menubar = no, scrollbars = no, resizable = no," +
                  "width = 300, height = 200");
      }
 
      function zipCheck()
      {
            url = "zipCheck.jsp?check=y";

            open(url, "post",
                  "toolbar = no, status = yes, menubar = no," + 
                  "scrollbars = yes, directories = no," +
                  "width = 500, height = 300");
      }

</script>
<script language="Javascript">

function checkemailaddy(){
        if (frm.email_select.value == '1') {
            frm.ip_domain.readonly = false;
            frm.ip_domain.value = '';
            frm.ip_domain.focus();
        }
        else {
            frm.ip_domain.readonly = true;
            frm.ip_domain.value = frm.email_select.value;
        }
    }
</script>
<script>

function inputchk(){
	
	 var right = 0;
	 var chk1 = /\d/;
	 var chk2 = /[a-z]/i;
	 var ip_mid = document.frm.ip_mid.value;
	 var ip_pwd = document.frm.ip_pwd.value;
	 var ip_pwdchk = document.frm.ip_pwdchk.value;
	 var ip_email = document.frm.ip_email.value;
	 
	 if(ip_mid.length<5 || ip_mid==null){
		  alert("아이디를 입력하십시오(5~12글자)");
		  return false;
     }
	 
	 if(ip_pwd.length<6 || ip_pwd==null){
	  alert("비밀번호를 입력하십시오(6글자이상)");
	  return false;
	 }
	 
	 if (chk1.test(ip_pwd) && chk2.test(ip_pwd)){
	 }else{
	  alert("비밀번호는 영어, 숫자 조합입니다.");
	  return false;
	 }
	 
	 if(ip_pwd != ip_pwdchk || ip_pwd==""){
	  alert("비밀번호가 동일한지 확인하시오");
	  return false;
	 }
	 
	 var num_regx = /^[0-9]*$/;
	 var ip_phone = document.frm.ip_phone.value;
	 if(ip_phone.length==0 || ip_phone==null){
	  alert("핸드폰번호를 입력하십시오");
	  return false;
	 }
	 
	 if (!num_regx.test(ip_phone)){
	  alert("핸드폰번호는 숫자만 입력가능합니다");
	  return false;
	 }
	 
	 var ip_irum = document.frm.ip_irum.value;
	 if(ip_irum.length==0 || ip_irum==null){
	  alert("이름을 입력하십시오");
	  return false;
	 }
	 
	 
	 
	 var ip_email = document.frm.ip_email.value;
	 var ip_domain = document.frm.ip_domain.value;
	 var regx = /^[a-zA-Z0-9._-]/;

	 
	 if(ip_email.length==0 || ip_email==null){
	  alert("이메일을 입력하십시오");
	  return false;
	 }
	 if (!regx.test(ip_email)){
	  alert("이메일은 영어, 숫자만 입력가능합니다.");
	  document.frm.ip_email.focus();
	  return false;
	 }
	/* if(ip_email2.length==0 || email2==null){
	  alert("이메일을 입력하십시오");
	  return false;
	 }
	 if (!regx.test(email2)){
	  alert("이메일은 영어, 숫자만 입력가능합니다.");
	  document.form.email2.focus();
	  return false;
	 }*/
	 
	 var ip_code = document.frm.ip_code.value;
	 if(ip_code.length==0 || ip_code==null){
		  alert("우편번호를 입력하십시오");
		  return false;
		 }
	 
	 var ip_address = document.frm.ip_address.value;
	 if(ip_address.length==0 || ip_address==null){
	  alert("주소를 입력하십시오");
	  return false;
	 }
	 document.frm.submit();
	}
	/*function updateCancel(){
	 location.href="readyinfo.jsp"; 
	}*/
	
	function pwdchk(){
	 var ip_pwd = document.frm.ip_pwd.value;
	 var ip_pwdchk = document.frm.ip_pwdchk.value;
	 if (ip_pwdchk.length == 0 || ip_pwdchk == null) {
	  document.frm.chk.value = "비밀번호를 입력하세요";
	  right = 0;
	 } else if (ip_pwd != ip_pwdchk) {
	  document.frm.chk.value = "비밀번호가 다름니다.";
	  right = 0;
	 } else {   
	  document.frm.chk.value = "비밀번호가 동일합니다.";
	  right = 1;
	 }
	 return;
	}


</script>
</head>
<body>


<form action="index.jsp?sub=login/input_result_person.jsp" name="frm" method="post" onSubmit = "return inputchk()">
<table id="bo1" width = "800" border = "1" cellspacing = "0" cellpadding = "3" align = "center">
<tr>
      <td id="up" colspan = "2" height = "39">
      <font size = "+1"><h2><b>회원가입</b></h2></font></td>
</tr>
<tr>
      <td width = "200" ><b>아이디 입력</b></td>
      <td width = "600" ></td>
</tr>
<tr>
      <td width = "200">사용자 ID</td>
      <td width = "600">
            <input type = "text" name = "ip_mid" size = "15" maxlength = "12" placeholder="5~12글자">
            <input id="col3" type = "button" name = "confirm_id" value = "ID중복확인" onclick = "openConfirmid(this.form)" style="cursor:pointer">
      </td>
</tr>
<tr>
      <td width = "200">비밀번호</td>
      <td width = "600">
            <input type = "password" name = "ip_pwd" size = "15" maxlength = "12" placeholder="영문+숫자 6자이상">
      </td>
</tr>
<tr>
      <td width = "200">비밀번호 확인</td>
      <td width = "600">
            <input type = "password" name = "ip_pwdchk" size = "15" maxlength = "12" onblur = "pwdchk()">
            <input type="text" name="chk" value="동일한 비밀번호를 입력하세요" size = "30" readonly="readonly" id="war">
      </td>
</tr>
<tr>
      <td width = "200" ><h2><b>개인정보 입력</b></h2></td>
      <td width = "600" ></td>
</tr>
<tr>
      <td width = "200">사용자 이름</td>
      <td width = "600">
            <input type = "text" name = "ip_irum" size = "15" maxlength = "10">
      </td>
</tr>

<tr>
      <td width = "200">연락처</td>
      <td width = "600">
            <input type = "text" name = "ip_phone" size = "15">
      </td>
</tr>

      <td width = "200">E-Mail</td>
      <td width = "600">
            <input id="ip_email" name="ip_email" type = "text" size = "15" maxlength = "30">@<input id="ip_domain" name="ip_domain" type="text">
             <select id="email_select" name="email_select" onchange="checkemailaddy();" style="height: 32px;">도메인선택
              <option value="nate.com">nate.com</option>
              <option value="naver.com">naver.com</option>
              <option value="hanmail.com">hanmail.com</option>
              <option value="gmail.com">gmail.com</option>
              <option value="1">직접입력</option>
             </select>
      </td>
</tr>

<tr>
      <td width = "200">우편번호</td>
      <td width = "600">
            <input type = "text" name = "ip_code" size = "7">
            <input id="col2" type = "button" value = "우편번호찾기" onClick = "zipCheck()" style="cursor:pointer">
           </td>
</tr>
<tr>
      <td width = "200">주소</td>
      <td width = "600">
            <input type = "text" name = "ip_address" size = "70" placeholder="주소를 적어 주세요.">
      </td>
</tr>
<tr>
      <td id="col" colspan = "2" align = "center">
            <input id="go" type = "submit" name = "confirm" value = "등록" style="cursor:pointer">
            <input type = "reset" name = "reset" value = "다시입력" style="cursor:pointer">
            <input type = "button" value = "취소" onclick="btnBack()" style="cursor:pointer">
      </td>
</tr>
</table>
</form>
</body>
<script>
function btnBack(){
	history.back();
}
</script>

</html>