<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
#log{
  height: 120px;
  text-align: center;
  background-color: #f8f8f8;
  padding-top: 50px;
}
#h_modi{
  border: 0 solid ;
  width: 100px;
  float: left;
  margin-left: 250px;
}
#h_m{
  margin-left: 20px;
  height: 25px;
}
#back{
  width:90px;
  height: 80px;
  float: right;
  margin-right: 150px;
  background-color: #5c5c5c;
  color: white;
  font-weight:bold;
  border: none;
  outline: none;
}
#search{
  width:120px;
  height: 80px;
  float: right;
  background-color: #da2226;
  color: white;
  font-weight:bold;
  border: none;
  outline: none;
  margin-right: 20px;
}
#wrap{
  padding-top: 80px;
  border-top: 5px solid gray;

}
</style>
<script language = "javascript">
function checkIt()
{
      var frm = eval("document.frm");

      if(!frm.ip_irum.value)
      {
            alert("이름을 입력하세요");
            return false;
      }
      if(!frm.ip_email.value)
      {
            alert("이메일을 입력하세요");
            return false;
      }

}
</script>
</head>
<body>
<div id="log">
<h1>아이디 찾기</h1>
<h5>당신의 꿈을 응원하는 아카데미 코리아에 오신걸 환영합니다.</h5>
</div>
<br/><br/><br/><br/>
<form name="frm" action='index.jsp?sub=login/searchMid_result.jsp' method="post" onSubmit = "return checkIt()">
<div id="wrap">
<input id="back" type="button" value="뒤로" onclick="btnBack()" style="cursor:pointer">
<input id="search" type="submit" value="아이디 찾기" style="cursor:pointer">
<div id='h_modi'>이름: </div><input id="h_m" type="text" name="ip_irum" placeholder="Name"><br/><br/>
<div id='h_modi'>이메일: </div><input id="h_m" type="text" name="ip_email" placeholder="Email">
</div>
</form>
</body>
<script>
function btnBack(){
	history.back();
}
</script>
</html>