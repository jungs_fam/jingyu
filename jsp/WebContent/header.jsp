<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
#i_img{
   margin:15px;
   width : 250px;
   height : 100px;
   background-image: url('img/Plogo.png');
   background-size: 100% 100%;
   float: left;
}
#i_hmenu{
   width : 600px;
   padding-top: 50px;
   padding-left: 300px;
}
#i_log{
  float: right;
  padding-right: 5px;
  padding-top: 10px;
}
#i_log a{
  font-weight: bold;
  font-size: 12px;
  color: #999;
  text-decoration: none;
}
.green_window {
	display: inline-block;
	width: 366px; height: 34px;
	border: 3px solid #3b78e7;
	background: white;
	margin-left: 70px;
}
.input_text {
	width: 348px; height: 21px;
	margin: 6px 0 0 9px;
	border: 0;
	line-height: 21px;
	font-weight: bold;
	font-size: 16px;
	outline: none;
}
.sch_smit {
	width: 54px; height: 40px;
	margin: 0; border: 0;
	vertical-align: top;
	background: #3b78e7;
	color: white;
	font-weight: bold;
	border-radius: 1px;
	cursor: pointer;
}
.sch_smit:hover {
	background: #66b2FF;
}
#as{
margin-left: 20px;

}
</style>
<script src='mypage/o_all.js'></script>
</head>
<body>

<div id="i_log">
 <%
 
 String ip_mid =(String)session.getAttribute("ip_mid");
 String im_id = (String)session.getAttribute("im_id");

  //세션에 값이 없으면 로그인 있으면 로그아웃 표시
 if(ip_mid==null){
 out.print("<label><a href='index.jsp?sub=login/login.jsp'><img src='img/Key.png' height='16px' width='16px'>로그인</a></label>"+"　"+"<label><a href='index.jsp?sub=login/terms.jsp'><img src='img/ConferenceCall.png' height='16px' width='16px'>회원가입</a></label>");
 }else if(ip_mid!=null){

 out.print("<label><a href='index.jsp?sub=login/logout.jsp'><img src='img/Unlock.png' height='16px' width='16px'>로그아웃</a></label>"+"　"+"<label>");%>

 <a href='#' onclick = "o_mmit('<%=ip_mid%>')"><img src='img/Manager.png' height='16px' width='16px'>MyPage</a>
  <% 
 
  out.print ("</label>");
	 
 }if(im_id!=null){

 out.print("<label><a href='index.jsp?sub=login/logout.jsp'><img id='as' src='img/Unlock.png' height='16px' width='16px'>관리자 로그아웃</a></label>"+"　"+"<label>");%>

 <a href='#' onclick = "o_mmit('<%=ip_mid%>')"><img src='img/Manager.png' height='16px' width='16px'><font color="#da2226">관리자</font></a>
  <% 
 
  out.print ("</label>");
	 
 }

 %>
 </div>
<div id='i_img' style="cursor:pointer" onclick="location.href='index.jsp'">
</div>

<div id="i_hmenu">

<span class='green_window'>
	<input type='text' class='input_text' />
</span>
<button type='submit' class='sch_smit'>검색</button>
</div>

<%@include file="storeForm.jsp" %>

</body>
</html>